import { post, getUserInfo } from './api';

export function checkAuth(nextState, replace, callback, returnUser) {
    getUserInfo()
        .then(user => {
            callback(returnUser && user);
        })
        .catch(err => {
            console.error(err);
            replace('/login');
            return callback();
        });
}

export function checkRole(role, nextState, replace, callback) {
    checkAuth(nextState, replace, user => {
        if (user.role !== role) replace('/dashboard');

        callback();
    }, true);
}

export function login(fields) {
    return post('/login', fields)
        .then(res => {
            if (res.status !== 200) throw new Error('Wrong credentials');
            return res.json();
        });
}

export function register(fields) {
    let ok;
    return post('/register', fields)
        .then(res => {
            ok = res.ok;
            return res.text();
        })
        .then(res => {
            if (!ok) throw new Error(res);
        });
}

export function requestPasswordReset(email) {
    return post('/recover', { email })
        .then(res => {
            if (res.status !== 200) throw new Error('Email not found');
        });  
}

export function resetPassword(password, token) {
    return post('/reset-password', { password, token })
        .then(res => {
            if (res.status !== 200) throw new Error('Failed to reset password');
        });
}
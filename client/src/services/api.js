export function post(url, body, method) {
    return fetch(url, {
            method: method || 'POST',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(body)
        });
}

export function get(url, json) {
    return fetch(url, { credentials: 'same-origin' }).then(res => json ? res.json() : res);
}

export function getUserInfo(id) {
    return get(`/users/${id || 'me'}`)
        .then(res => {
            if (!res.ok) throw new Error('User not found');
            return res.json();
        });
}

export function updateUser(data) {
    return post('/users/me', data, 'PUT')
        .then(res => {
            if (res.status !== 200) throw new Error('Wrong credentials');
        });
}

export function getUsersList() {
    return get('/users', true);
}

export function requestQuery(query) {
    let ok;
    return post('/query', { query })
        .then(res => {
            ok = res.ok;
            return res.json();
        })
        .then(res => {
            if (ok) {
                return res;
            } else {
                throw new Error(res.message);
            }
        });
}

export function createRace(data) {
    return post('/races', data)
        .then(res => {
            if (!res.ok) throw new Error();
            return res.json();
        });
}

export function getRaces() {
    return get('/races', true);
}

export function createEntry(data) {
    return post('/entries', data)
        .then(res => {
            if (!res.ok) throw new Error();
            return res.json();
        });
}

export function getEntries() {
    return get('/entries', true);
}

export function getEntryOfRace(raceId) {
    return get('/entries?by=race&raceId=' + raceId)
        .then(res => {
            if (!res.ok) throw new Error('Entry not found');
            return res.json();
        });
}

export function addEntryDetails(entryId, data) {
    return post('/entries/' + entryId, data, 'PUT')
        .then(res => {
            if (!res.ok) throw new Error();
            return res.json();
        });
}

export function submitPaymentData(entryId, data) {
    return post('/entries/' + entryId + '/payment', data)
        .then(res => {
            if (!res.ok) throw new Error();
            if (res.redirected) window.open(res.url);
        })
}
import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton';

import SingleForm from './SingleForm';
import { parseQueryString } from '../utils';

export default class PaymentPage extends Component {
    constructor(props) {
        super();

        let { amount, callbackUrl } = parseQueryString(props.location.search);
        callbackUrl = callbackUrl ? decodeURIComponent(callbackUrl) : '/';

        this.state = { amount, callbackUrl };
    }

    processPayment() {
        const { history } = this.props;
        const { callbackUrl } = this.state;

        history.push(callbackUrl);
    }

    cancelPayment() {
        window.close();
    }

    render() {
        return (
            <SingleForm onSubmit={this.onSubmit}>
                <h2>Your Lovely Bank</h2>
                <h3 className="text-centered">Would you like to issue a payment of ${this.state.amount} ?</h3> 
                
                <RaisedButton
                    className="form-button margin-s-top"
                    label="Confirm"
                    primary={true}
                    fullWidth={true}
                    onTouchTap={this.processPayment.bind(this)} />

                <RaisedButton
                    className="form-button margin-s-top"
                    label="Cancel"
                    secondary={true}
                    fullWidth={true}
                    onTouchTap={this.cancelPayment.bind(this)} />
            </SingleForm>
        )
    }
}
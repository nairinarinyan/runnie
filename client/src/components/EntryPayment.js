import React, { Component } from 'react';

import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import RightChevron from 'material-ui/svg-icons/navigation/chevron-right';

import { omit } from '../utils';
import { getEntryIfNotFound } from './EntryDetailsForm';

export default class EntryPayment extends Component {
    constructor() {
        super();
        this.state = {
            cardNumber: '',
            cardHolderName: '',
            expirationMonth: '',
            expirationYear: '',
            cvc: '',
            entry: null
        };
    }
    
    componentDidMount() {
        getEntryIfNotFound(this.props, this.state, this.setState.bind(this));
    }

    componentWillReceiveProps(props) {
        getEntryIfNotFound(props, this.state, this.setState.bind(this));
    }

    onChange(field, evt, value) {
        this.setState({
            [field]: value
        });
    }

    onSubmit(evt) {
        evt.preventDefault();
        const paymentData = omit(this.state, 'entry');

        let [,amount] = this.state.entry.price.split(' ');
        amount = parseFloat(amount);
        paymentData.amount = amount;

        this.props.onSubmit(this.state.entry.id, paymentData);
    }

    formatPriceAmount(price) {
        const [packageName, amount] = price.split(' ');
        return <p>{packageName} <strong>{amount}</strong></p>;
    }

    render() {
        const { cardNumber, cardHolderName, expirationMonth, expirationYear, cvc, entry } = this.state;

        return (
            <section className="flex-column">
                <div className="payment-details">
                    <h3>Payment</h3>
                    {entry && this.formatPriceAmount(entry.price)}
                </div>

                <form className="adaptive" onSubmit={this.onSubmit.bind(this)}>
                    <TextField
                        value={cardNumber}
                        onChange={this.onChange.bind(this, 'cardNumber')}
                        floatingLabelText="Card Number"
                        type="number"
                        errorText={false}
                        fullWidth={true}
                        name="card-number" />

                    <TextField
                        value={cardHolderName}
                        onChange={this.onChange.bind(this, 'cardHolderName')}
                        floatingLabelText="Card holder name"
                        errorText={false}
                        fullWidth={true}
                        name="card-holder-name" />

                    <div className="card-expiry-date">
                        <TextField
                            onChange={this.onChange.bind(this, 'expirationMonth')}
                            floatingLabelText="Month"
                            type="number"
                            errorText={false}
                            name="card-expiry-month" />
                        <div className="delimiter">
                            <span>/</span>
                        </div>
                        <TextField
                            onChange={this.onChange.bind(this, 'expirationYear')}
                            type="number"
                            floatingLabelText="Year"
                            errorText={false}
                            name="card-holder-year" />
                    </div>

                    <div className="cvc">
                        <TextField
                            value={cvc}
                            onChange={this.onChange.bind(this, 'cvc')}
                            floatingLabelText="CVV/CVC"
                            type="number"
                            errorText={false}
                            name="cvc" />
                    </div>

                    <FloatingActionButton className="submit-button" type="submit">
                        <RightChevron />
                    </FloatingActionButton>
                </form>
            </section>
        );
    }
}

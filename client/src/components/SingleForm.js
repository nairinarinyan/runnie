import React, { Component } from 'react';
import Paper from 'material-ui/Paper';

const SingleForm = ({ onSubmit, children }) => (
    <div className="fullscreen-centered">
        <Paper zDepth={2} className="form-window" >
            <form onSubmit={onSubmit}>
                {children}
            </form>
        </Paper>
    </div>
);

export default SingleForm;
import React, { Component } from 'react';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn
} from 'material-ui/Table';

import { parseDateTime } from '../utils';

export default class RacesTable extends Component {
    static defaultProps = {
        races: []
    }

    renderRows() {
        return this.props.races.map((race, i) =>
            <TableRow className="table-row" key={i}>
                <TableRowColumn>{race.name}</TableRowColumn>
                <TableRowColumn>{race.start}</TableRowColumn>
                <TableRowColumn>{race.finish}</TableRowColumn>
                <TableRowColumn>{race.distance}m</TableRowColumn>
                <TableRowColumn>{parseDateTime(race.date)}</TableRowColumn>
            </TableRow>
        );
    }

    selectRow(idx) {
        const { races, selectRow } = this.props;
        selectRow && selectRow(races[idx]);
    }

    render() {
        return (
            <Table className="table" onRowSelection={this.selectRow.bind(this)}>
                <TableHeader
                    adjustForCheckbox={false}
                    displaySelectAll={false}>
                    <TableRow>
                        <TableHeaderColumn>Name</TableHeaderColumn>
                        <TableHeaderColumn>Start</TableHeaderColumn>
                        <TableHeaderColumn>Finish</TableHeaderColumn>
                        <TableHeaderColumn>Distance</TableHeaderColumn>
                        <TableHeaderColumn>Date</TableHeaderColumn>
                    </TableRow>
                </TableHeader>
                <TableBody displayRowCheckbox={false}>
                    {this.renderRows()}
                </TableBody>
            </Table>
        );
    }
}

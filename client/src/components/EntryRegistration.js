import React, { Component } from 'react';

import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import IconButton from 'material-ui/IconButton';
import Checkbox from 'material-ui/Checkbox';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import DateIcon from 'material-ui/svg-icons/action/date-range';
import RightChevron from 'material-ui/svg-icons/navigation/chevron-right';

import Dropdown from './Dropdown';

const fields = {
    prefix: ['Mr', 'Ms', 'Mrs', 'Dr', 'Prof'],
    price: ['Standard 40€', 'Student 23€', 'Member 35€'],
    sex: ['Male', 'Female'],
    shirtSize: ['Child', 'XS', 'S', 'M', 'L', 'XL'],
    pace: ['4:00-4:30', '4:30-5:30', '5:00-5:30', '5:30-6:00']
};

export default class EntryRegistration extends Component {
    constructor() {
        super();
        this.state = {
            runnerId: '',
            prefix: fields.prefix[0],
            firstName: '',
            lastName: '',
            birthday: new Date().toLocaleDateString(),
            price: fields.price[0],
            sex: fields.sex[0],
            shirtSize: fields.shirtSize[0],
            pace: fields.pace[0],
        };
    }

    onChange(field, evt, val) {
        const value = typeof val === 'number' ? fields[field][val] : val;

        this.setState({
            [field]: value
        });
    }

    enterDate({ target }) {
        const date = new Date(target.value);

        this.setState({
            birthday: target.value
        });
    }

    getDateForDatepicker() {
        const { birthday } = this.state;
        const date = new Date(birthday);

        return isFinite(date) ? date : new Date();
    }

    onDatepickerChange(_, dateVal) {
        dateVal.setHours(12);

        this.setState({
            birthday: dateVal.toLocaleDateString()
        });
    }

    onSubmit(evt) {
        evt.preventDefault();
        this.props.onSubmit(this.state);
    }

    render() {
        const { race } = this.props;
        const {
            runnerId,
            firstName,
            lastName,
            prefix,
            price,
            birthday,
            sex,
            shirtSize,
            pace,
            debut,
            bibNumber,
            phoneNumber,
            termsAccepted
        } = this.state;

        return (
            <section className="flex-column">
                <h3>{race && race.name}</h3>

                <form className="adaptive" onSubmit={this.onSubmit.bind(this)}>
                    <TextField
                        value={runnerId}
                        onChange={this.onChange.bind(this, 'runnerId')}
                        floatingLabelText="Runner ID"
                        errorText={false}
                        fullWidth={true}
                        name="runner-id" />

                    <Dropdown
                        value={prefix}
                        field="Prefix"
                        options={fields.prefix}
                        fullWidth={true}
                        onChange={this.onChange.bind(this, 'prefix')} />

                    <TextField
                        value={firstName}
                        onChange={this.onChange.bind(this, 'firstName')}
                        floatingLabelText="First Name"
                        fullWidth={true}
                        name="first-name" />

                    <TextField
                        value={lastName}
                        onChange={this.onChange.bind(this, 'lastName')}
                        fullWidth={true}
                        floatingLabelText="Last Name"
                        name="family-name" />

                    <div className="form-row">
                        <TextField
                            value={this.state.birthday}
                            onChange={this.enterDate.bind(this)}
                            floatingLabelText="Birthday"
                            className="date-textfield"
                            name="birthday" />

                        <IconButton className="datepicker-icon" onTouchTap={() => this.datePicker.openDialog()}>
                            <DateIcon />
                        </IconButton>

                        <DatePicker
                            onChange={this.onDatepickerChange.bind(this)}
                            value={this.getDateForDatepicker()}
                            autoOk={true}
                            ref={dp => this.datePicker = dp}
                            style={{ display: 'none' }}
                            name="birthday" />
                    </div>

                    <Dropdown
                        value={price}
                        field="Price"
                        options={fields.price}
                        fullWidth={true}
                        onChange={this.onChange.bind(this, 'price')} />

                    <div className="sex-shirt-size-holder">
                        <Dropdown
                            value={sex}
                            field="Sex"
                            options={fields.sex}
                            onChange={this.onChange.bind(this, 'sex')} />

                        <Dropdown
                            value={shirtSize}
                            field="T-Shirt Size"
                            options={fields.shirtSize}
                            onChange={this.onChange.bind(this, 'shirtSize')} />
                    </div>

                    <Dropdown
                        value={pace}
                        field="Pace"
                        options={fields.pace}
                        fullWidth={true}
                        onChange={this.onChange.bind(this, 'pace')} />

                    <Checkbox
                        className="checkbox"
                        value={debut}
                        onCheck={this.onChange.bind(this, 'debut')}
                        label="Debut" />

                    <TextField
                        value={bibNumber}
                        onChange={this.onChange.bind(this, 'bibNumber')}
                        floatingLabelText="Bib number"
                        fullWidth={true}
                        name="bib-number" />

                    <TextField
                        value={phoneNumber}
                        onChange={this.onChange.bind(this, 'phoneNumber')}
                        floatingLabelText="Emergency phone number"
                        fullWidth={true}
                        name="phone-number" />

                    <div className="legal">
                        <a href="http://google.com" target="_blank">Terms and conditions</a>
                        <Checkbox
                            className="checkbox"
                            value={termsAccepted}
                            onCheck={this.onChange.bind(this, 'termsAccepted')}
                            label="I accept the terms" />
                    </div>

                    <FloatingActionButton className="submit-button" type="submit" disabled={!termsAccepted}>
                        <RightChevron />
                    </FloatingActionButton>
                </form>
            </section>
        );
    }
}

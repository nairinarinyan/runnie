import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

import Drawer from 'material-ui/Drawer';
import { List, ListItem, makeSelectable } from 'material-ui/List';

import PeopleIcon from 'material-ui/svg-icons/social/people';
import StorageIcon from 'material-ui/svg-icons/device/storage';

const SelectableList = makeSelectable(List);

class LeftMenu extends Component {
    state = {
        open: false,
        activeItemIndex: 0
    }

    setStateFromPathname(menuItems, pathname) {
        const activeItemIndex = menuItems.findIndex(item => pathname.startsWith(item.path));

        if (~activeItemIndex && this.state.activeItemIndex !== activeItemIndex) {
            this.setState({ activeItemIndex });
        }
    }

    componentWillMount() {
        const { menuItems, history, location: { pathname } } = this.props;

        this.setStateFromPathname(menuItems, pathname);

        history.listen((location, action) => {
            const { pathname } = location;
            this.setStateFromPathname(menuItems, pathname);
        });
    }
    
    selectMenuItem(evt, activeItemIndex) {
        const { menuItems, history } = this.props;

        this.setState({ activeItemIndex }, () => {
            const { activeItemIndex } = this.state;
            history.push(menuItems[activeItemIndex].path);
        });
    }

    render() {
        const { drawerOpen, menuItems } = this.props;

        return (
            <MuiThemeProvider muiTheme={getMuiTheme(darkBaseTheme)}>
                <Drawer className="drawer" open={drawerOpen}>
                    <SelectableList
                        onChange={this.selectMenuItem.bind(this)}
                        value={this.state.activeItemIndex}>

                        {menuItems.map((item, i) =>
                            <ListItem
                                key={i}
                                value={i}
                                primaryText={item.text}
                                leftIcon={item.icon}
                            />
                        )}
                    </SelectableList>
                </Drawer>
            </MuiThemeProvider>
        );
    }
}

export default withRouter(LeftMenu);
import React, { Component } from 'react';
import Snackbar from 'material-ui/Snackbar';

const Toaster = ({ show, message, type }) => (
    <Snackbar
        open={show}
        message={message}
        bodyStyle={styles[type]}
        contentStyle={styles[type]}
        autoHideDuration={1500}
    />
);

const commonStyles = {
    textAlign: 'center'
};

const styles = {
    error: {
        ...commonStyles,
        backgroundColor: '#E83151',
    },
    warning: {

    },
    success: {

    }
};

export default Toaster;
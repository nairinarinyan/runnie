import React, { Component } from 'react';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn
} from 'material-ui/Table';

import CheckIcon from 'material-ui/svg-icons/navigation/check';
import CloseIcon from 'material-ui/svg-icons/navigation/close';

export default class EntriesTable extends Component {
    renderRows() {
        const { entries } = this.props;

        return entries && entries.map(({ id, paid, race: { name, distance, date} }) =>
            <TableRow className="table-row" key={id}>
                <TableRowColumn>{name}</TableRowColumn>
                <TableRowColumn>{distance}m</TableRowColumn>
                <TableRowColumn>{new Date(date).toDateString()}</TableRowColumn>
                <TableRowColumn>
                    {paid ? <CheckIcon color="#6CC551" /> : <CloseIcon color="#DD403A" />}
                </TableRowColumn>
            </TableRow>
        );
    }

    selectRow(idx) {
        const { entries, selectEntry } = this.props;
        selectEntry(entries[idx]);
    }

    render() {
        return (
            <section className="main-view">
                <Table className="table" onRowSelection={this.selectRow.bind(this)}>
                    <TableHeader
                        adjustForCheckbox={false}
                        displaySelectAll={false}>
                        <TableRow>
                            <TableHeaderColumn>Race</TableHeaderColumn>
                            <TableHeaderColumn>Distance</TableHeaderColumn>
                            <TableHeaderColumn>Date</TableHeaderColumn>
                            <TableHeaderColumn>Paid</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody displayRowCheckbox={false}>
                        {this.renderRows()}
                    </TableBody>
                </Table>
            </section>
        );    
    }
}


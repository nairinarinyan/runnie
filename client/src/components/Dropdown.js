import React, { Component } from 'react';

import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

const Dropdown = ({ field, value, options, onChange, fullWidth, errorText }) => (
    <SelectField
        value={options.indexOf(value)}
        fullWidth={fullWidth || false}
        onChange={onChange}
        errorText={errorText}
        floatingLabelText={field}>

        {options.map((value, i) =>
            <MenuItem key={i} value={i} primaryText={value} />
        )}
    </SelectField>
);

export default Dropdown;
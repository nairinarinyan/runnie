import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import RaisedButton from 'material-ui/RaisedButton';

export default class Confirmation extends Component {
    onSubmit(evt) {
        evt.preventDefault();
        this.props.onSubmit();
    }

    render() {
        return (
            <section className="flex-column">
                <h3>Thank you! Your payment is accepted</h3>

                <Link to="/entrylist">
                    <RaisedButton label="Go to your entries" primary={true} /> 
                </Link>
            </section>
        );
    }
}

import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import RightChevron from 'material-ui/svg-icons/navigation/chevron-right';

import Dropdown from './Dropdown';
import { pick, omit } from '../utils';
import countries from '../../../data/countries.json';

export function getEntryIfNotFound(props, state, _setState) {
    const { race, activeEntry, getEntryOfRace } = props;

    if (activeEntry) {
        return _setState({
            entry: activeEntry
        });
    }

    if (!state.entry && race) {
        getEntryOfRace(race.id);
    }
}

export default class EntryDetails extends Component {
    state = {
        country: '',
        city: '',
        streetAddress: '',
        postalCode: '',
        entry: null,
    }

    componentDidMount() {
        getEntryIfNotFound(this.props, this.state, this.setState.bind(this));
    }

    componentWillReceiveProps(props) {
        getEntryIfNotFound(props, this.state, this.setState.bind(this));
    }

    onChange(type, evt, val) {
        let value = typeof val === 'number' ? countries[val].name : val;

        this.setState({
            [type]: value
        });
    }

    onSubmit(evt) {
        evt.preventDefault();
        const detailsData = omit(this.state, 'entry');

        this.props.onSubmit(this.state.entry.id, detailsData);
    }

    render() {
        const { country, city, streetAddress, postalCode } = this.state;

        return (
            <section className="flex-column">
                <h3>Details</h3>

                <form className="adaptive" onSubmit={this.onSubmit.bind(this)}>
                    <Dropdown
                        value={country}
                        field="Country"
                        options={countries.map(c => c.name)}
                        fullWidth={true}
                        onChange={this.onChange.bind(this, 'country')} />

                    <TextField
                        value={city}
                        onChange={this.onChange.bind(this, 'city')}
                        floatingLabelText="City"
                        errorText={false}
                        fullWidth={true}
                        name="city" />

                    <TextField
                        value={streetAddress}
                        onChange={this.onChange.bind(this, 'streetAddress')}
                        floatingLabelText="Street Address"
                        errorText={false}
                        fullWidth={true}
                        name="streetAddress" />

                    <TextField
                        value={postalCode}
                        onChange={this.onChange.bind(this, 'postalCode')}
                        floatingLabelText="Postal Code"
                        errorText={false}
                        fullWidth={true}
                        name="postalCode" />

                    <FloatingActionButton className="submit-button" type="submit">
                        <RightChevron />
                    </FloatingActionButton>
                </form>
            </section>
        );
    }
}

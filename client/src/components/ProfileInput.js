import React, { Component } from 'react';

export default class ProfileInput extends Component {
    selectorInput(options, value, onChange) {
        return (
            <div className="input">
                <select value={value} onChange={onChange}>
                    {options.map((option, i) =>
                        <option key={i} value={option.name}>{option.name}</option>
                    )}
                </select>
            </div>
        );
    }

    textInput(value, onChange, disabled) {
        return (
            <div className="input">
                <input type="text" value={value || ''} onChange={onChange} disabled={disabled}/>
            </div>
        );
    }

    dateInput(value, onChange) {
        const { day, month, year } = value;

        return (
            <div className="input">
                <input type="number" value={day} onChange={onChange.bind(this, 'day')}/>
                <input type="number" value={month} onChange={onChange.bind(this, 'month')}/>
                <input type="number" value={year} onChange={onChange.bind(this, 'year')}/>
            </div>
        );
    }

    render() {
        const { type, field, value, options, onChange, disabled } = this.props;

        return (
            <div className="profile-input">
                <label>{field}</label>
                {type === 'selector' ?
                    this.selectorInput(options, value, onChange) :
                type === 'date' ?
                    this.dateInput(value, onChange) :
                    this.textInput(value, onChange, disabled)
                }
            </div>
        );
    }
}

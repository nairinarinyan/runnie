import React, { Component } from 'react';
import { parseSeconds } from '../utils'

export default class Timer extends Component {
    constructor(props) {
        super();
        this.state = {
            time: props.time
        };
    }

    componentDidMount() {
        this.timerId = setInterval(() => {
            const time = this.state.time - 1;

            this.setState({ time }, () => this.props.onTick(time));
        }, 1000);
    }

    componentDidUpdate() {
        const { time } = this.state;
        const { onFinish } = this.props;

        if (time === 0) onFinish();
    }

    componentWillUnmount() {
        clearInterval(this.timerId);
    }

    render() {
        return (
            <div className="timer">
                <p>{parseSeconds(this.state.time)}</p>
            </div>
        );
    }
}
import React, { Component } from 'react';
import ProfileInput from './ProfileInput';

import countries from '../../../data/countries.json';

export default class ProfileInfo extends Component {
    constructor() {
        super();
        this.state = {};
    }

    componentDidMount() {
        const { user } = this.props;
        const normalizedUser = { ...user };

        normalizedUser.birthday = this.formatDate(user.birthday);

        let { country } = normalizedUser;
        normalizedUser.country = country ? country.name : '';

        this.setState({ user: normalizedUser });
    }

    formatDate(dateString) {
        const date = new Date(dateString);

        const day = date.getDate();
        const month = date.getMonth() + 1;
        const year = date.getFullYear();

        return { day, month, year };
    }

    setDate(type, { target }) {
        const { user } = this.state;
        const { birthday } = user;

        this.setState({
            user: {
                ...user,
                birthday: {
                    ...birthday,
                    [type]: target.value
                } 
            }
        });
    }

    reformatDate(birthday) {
        const { day, month, year } = birthday;
        const date = new Date(year, month - 1, day);

        return date;
    }

    onChange(field, { target }) {
        const { user } = this.state;

        this.setState({
            user: {
                ...user,
                [field]: target.value
            }
        });
    }

    onSubmit(e) {
        e.preventDefault();
        const { user } = this.state;
        const userData = { ...user };

        userData.birthday = this.reformatDate(userData.birthday);

        this.props.updateUser(userData);
    }

    render() {
        const { user } = this.state;

        return (
            <section id="profile-info">
                <form onSubmit={::this.onSubmit}>
                    {user && 
                        <div className="input-wrapper">
                            <ProfileInput field="Id" value={user.id} disabled={true}/>
                            <ProfileInput field="Username" value={user.username} onChange={this.onChange.bind(this, 'username')}/>
                            <ProfileInput field="First Name" value={user.firstName} onChange={this.onChange.bind(this, 'firstName')}/>
                            <ProfileInput field="Last Name" value={user.lastName} onChange={this.onChange.bind(this, 'lastName')}/>
                            <ProfileInput field="Email" value={user.email} onChange={this.onChange.bind(this, 'email')}/>
                            <ProfileInput field="Phone Number" value={user.phoneNumber} onChange={this.onChange.bind(this, 'phoneNumber')}/>
                            <ProfileInput field="Address" value={user.address} onChange={this.onChange.bind(this, 'address')}/>
                            <ProfileInput field="Zip" value={user.zip} onChange={this.onChange.bind(this, 'zip')}/>
                            <ProfileInput field="Birthday" type="date" value={user.birthday} onChange={this.setDate.bind(this)}/>
                            <ProfileInput field="Country" type="selector" options={countries} value={user.country} onChange={this.onChange.bind(this, 'country')}/>
                        </div>
                    }

                    <div className="buttons">
                        <div className="button">
                            <button type="submit">Save</button>
                        </div>
                        <div className="button">
                            <a href="/logout">Logout</a>
                        </div>
                    </div>
                </form>
            </section>
        );
    }
}

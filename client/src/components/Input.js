import React, { Component } from 'react';

export default class Input extends Component {
    wrap(inputContent) {
        return (
            <div className="input-container">
                {inputContent}
            </div>
        );
    }

    inputContent() {
        const { type, value, error, placeholder, onChange, onBlur } = this.props;

        return(
            <div className={`input ${error ? 'error' : ''}`}>
                <input
                    type={type}
                    value={value}
                    placeholder={placeholder}
                    onChange={onChange}
                    onBlur={onBlur}
                />
                <div className="error-message">
                    <span>{error}</span>
                </div>
            </div>
        ); 
    }

    render() {
        const { noWrap } = this.props;

        return noWrap ? this.inputContent() : this.wrap(this.inputContent());
    }
}

import React, { Component } from 'react';

import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';

import LeftMenu from '../components/LeftMenu';

export default class DashboardLayout extends Component {
    constructor() {
        super();

        this.state = {
            drawerOpen: true
        };

        this.toggleDrawer = this.toggleDrawer.bind(this);
        this.logout = this.logout.bind(this);
    }

    componentDidMount() {
        this.setDrawerState(this.props.onMobile);
    }

    componentWillReceiveProps(props) {
        this.setDrawerState(props.onMobile);
    }

    setDrawerState(onMobile) {
        if (onMobile) {
            this.setState({ drawerOpen: false });
        }
    }

    toggleDrawer() {
        this.setState({
            drawerOpen: !this.state.drawerOpen
        });
    }

    logout() {
        location.replace('/logout');
    }

    render() {
        const { menuItems, onMobile } = this.props;
        const { drawerOpen } = this.state;

        return (
            <div>
                <AppBar
                    className="app-bar"
                    title="Runnie"
                    iconStyleLeft={{ display: !onMobile && 'none' }}
                    iconElementRight={<FlatButton label="Logout" />}
                    onRightIconButtonTouchTap={this.logout}
                    onLeftIconButtonTouchTap={this.toggleDrawer} />

                <LeftMenu
                    menuItems={menuItems}
                    drawerOpen={drawerOpen} />

                <div className={`main-view-container ${onMobile && 'mobile'}`} >
                    {this.props.children}
                </div>
            </div>
        );
    }
}
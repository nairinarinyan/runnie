import React, { Component } from 'react';

export default class AlertInfo extends Component {
    registrationSuccess(email) {
        return (
            <div className="message">
                <h3>Thanks! You need to confirm your email to complete the registration</h3>
                <h4>An email with the confirmation link has been sent to <span>{email}</span></h4>
            </div>
        );
    }

    passwordResetSuccess(email) {
        return (
            <div className="message">
                <h3>Thanks!</h3>
                <h4>An email with the password reset instructions has been sent to <span>{email}</span></h4>
            </div>
        );
    }

    renderMessage(type, email) {
        switch(type) {
            case 'registration':
                return this.registrationSuccess(email);
            case 'password':
                return this.passwordResetSuccess(email);
        }
    }

    render() {
        const { type, email } = this.props;

        return (
            <div id="alert-info">
                {this.renderMessage(type, email)}
            </div>
        );
    }
}

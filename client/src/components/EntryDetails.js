import React, { Component } from 'react';
import { parseDateTime, parseSeconds } from '../utils';

const entryFieldMap = {
    runnerId: 'Runner ID',
    price: 'Price',
    pace: 'Pace',
    debut: 'Debut',
    submissionTime: 'Submission Time',
    bibNumber: 'Bib Number'
};

const raceFieldMap = {
    start: 'Start',
    finish: 'Finish',
    distance: 'Distance',
    date: 'Date'
};

export default class EntryDetails extends Component {
    state = {
        remainingSeconds: 0
    }

    componentDidMount() {
        const { entry } = this.props;

        const subTime = new Date(entry.submissionTime);
        const remainingSeconds = 10 * 60 - (Date.now() - subTime) / 1000 << 0;

        this.setState({ remainingSeconds }, () => {
            this.timer = setInterval(() => {
                const remainingSeconds = this.state.remainingSeconds - 1;

                this.setState({ remainingSeconds });
            }, 1000); 
        });
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    matchAndGetValues(fieldMap, obj) {
        return Object.entries(fieldMap)
            .map(([fieldKey, fieldName]) => {
                const fieldValue = obj[fieldKey];

                return [fieldName, fieldValue];
            });
    }

    formatField(value) {
        if (typeof value === 'string' && isFinite(new Date(value))) {
            return parseDateTime(value);
        }

        if (typeof value === 'boolean') {
            return value.toString();
        }

        return value;
    }

    renderFields(entry) {
        const entryDetails = this.matchAndGetValues(entryFieldMap, entry);
        const raceDetails = this.matchAndGetValues(raceFieldMap, entry.race);

        return entryDetails.concat(raceDetails)
            .map(([fieldName, fieldValue], i) =>
                <div key={i} className="entry-detail">
                    <div>
                        <div className="field">
                            <h3>{fieldName}</h3>
                        </div>
                        <div className="field">
                            <p>{this.formatField(fieldValue)}</p>
                        </div>
                    </div>
                </div>
            );
    }

    renderPaymentSection(entry) {
        return <div>{parseSeconds(this.state.remainingSeconds)}</div> 
    }

    showExpired() {
        return <div>expired</div> 
    }

    render() {
        const { entry } = this.props;

        return (
            <section id="entry-details" className="main-view flex-column">
                <h2>{entry.race.name}</h2>

                <div className="details-container">
                    {this.renderFields(entry)}
                    {!entry.expired ?
                        this.renderPaymentSection(entry) :
                        this.showExpired()
                    }
                </div>
            </section>
        );
    }
}
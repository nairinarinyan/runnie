import React from 'react';

const NotFound = () => (
    <div className="fullscreen-centered">
        <h3>404 Page Not Found</h3>
    </div>
);

export default NotFound;

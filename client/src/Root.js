import React, { Component } from 'react';
import { Router, browserHistory } from 'react-router';
import { Provider } from 'react-redux';

import store from './modules/store';
import routes from './routes/index';

import App from './containers/App';

export default class Root extends Component {
    render() {
        return (
            <Provider store={store}>
                <App>
                    {routes}
                </App>
            </Provider>
        );
    } 
}
import { post } from '../services/api';
import { errorMessages } from '../../../lib/validator';

export function checkUsername(username) {
    if (!username) return Promise.reject(new Error(errorMessages.required));

    return post('/checkusername', { username })
        .then(res => {
            if (res.status !== 200) throw new Error(errorMessages.usernameTaken);
        });
}

export function checkRequired(value) {
    return value ? Promise.resolve() : Promise.reject(new Error(errorMessages.required));
}

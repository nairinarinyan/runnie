export const SELECT_RACE = 'races/SELECT_RACE';

export const GET_RACES = 'races/GET_RACES';
export const GET_RACES_SUCCESS = 'races/GET_RACES_SUCCESS';
export const GET_RACE_FAILED = 'races/GET_RACE_FAILED';

export const CREATE_RACE = 'races/CREATE_RACE';
export const CREATE_RACE_FAILED = 'races/CREATE_RACE_FAILED';

import * as ActionConstants from './races.constants';

export function selectRace(race) {
    return { type: ActionConstants.SELECT_RACE, race };
}

export function getRaces() {
    return { type: ActionConstants.GET_RACES };
}

export function getRacesSuccess(races) {
    return { type: ActionConstants.GET_RACES_SUCCESS, races };
}

export function getRaceFailed() {
    return { type: ActionConstants.GET_RACE_FAILED };
}

export function createRace(data) {
    return { type: ActionConstants.CREATE_RACE, data };
}

export function createRaceFailed() {
    return { type: ActionConstants.CREATE_RACE_FAILED };
}


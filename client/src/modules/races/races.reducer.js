import * as ActionConstants from './races.constants';
import * as LayoutConstants from '../layout/layout.constants';

export function races(state = {}, action) {
    switch(action.type) {
        case ActionConstants.SELECT_RACE:
            return { ...state, selectedRace: action.race };
        case LayoutConstants.SELECT_MENU_ITEM:
            return { ...state, selectedRace: null };
        case ActionConstants.GET_RACES_SUCCESS:
            return { ...state, racesList: action.races };
        default:
            return state;
    }
}

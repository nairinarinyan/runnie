import { takeLatest, call, put } from 'redux-saga/effects';
import * as ActionConstants from './races.constants';
import * as Actions from './races.actions';

import { createRace, getRaces } from '../../services/api';

export function* watchGetRaces() {
    yield takeLatest(ActionConstants.GET_RACES, handleGettingRaces);
}

export function* watchCreateRace() {
    yield takeLatest(ActionConstants.CREATE_RACE, handleCreateRace);
}

function* handleCreateRace(action) {
    try {
        yield call(createRace, action.data);
        yield put(Actions.getRaces());
    } catch (e) {
        console.log(e);
        yield put(Actions.createRaceFailed(e));
    }
}

function* handleGettingRaces(action) {
    try {
        const races = yield call(getRaces, action.data);
        yield put(Actions.getRacesSuccess(races));
    } catch (e) {
        console.log(e);
        yield put(Actions.getRaceFailed(e));
    }
}
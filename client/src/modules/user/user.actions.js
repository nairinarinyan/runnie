import * as ActionConstants from './user.constants';

export function getUserInfo(id) {
    return { type: ActionConstants.GET_INFO, id };
}

export function infoFetched(user) {
    return { type: ActionConstants.INFO_FETCHED, user };
}

export function fetchFailed(error) {
    return { type: ActionConstants.FETCH_FAILED, error };
}

// update
export function updateUser(data) {
    return { type: ActionConstants.UPDATE_USER, data };
}

export function userUpdateSuccess() {
    return { type: ActionConstants.USER_UPDATE_SUCCESS };
}

export function userUpdateFailed(error) {
    return { type: ActionConstants.USER_UPDATE_FAILED, error };
}

//admin
export function getUsersList() {
    return { type: ActionConstants.GET_USERS_LIST };
}

export function getUsersSuccess(users) {
    return { type: ActionConstants.GET_USERS_SUCCESS, users };
}

export function getUsersFailed() {
    return { type: ActionConstants.GET_USERS_FAILED };
}

export function requestQuery(query) {
    return { type: ActionConstants.REQUEST_QUERY, query };
}

export function queryResultSuccess(result) {
    return { type: ActionConstants.QUERY_RESULT_SUCCESS, result };
}

export function queryResultFailure(error) {
    return { type: ActionConstants.QUERY_RESULT_FAILURE, error };
}

export function resetQueryStatus() {
    return { type: ActionConstants.RESET_QUERY_STATUS };
}

import { takeLatest, call, put } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import * as ActionConstants from './user.constants';
import * as Actions from './user.actions';

import { getUserInfo, updateUser, getUsersList, requestQuery } from '../../services/api';

export function* watchUser() {
    yield takeLatest(ActionConstants.GET_INFO, handleGettingInfo);
}

export function* watchUserUpdate() {
    yield takeLatest(ActionConstants.UPDATE_USER, handleUserUpdate);
}

export function* watchUserList() {
    yield takeLatest(ActionConstants.GET_USERS_LIST, handleUsersFetch);
}

export function* watchQueryRequest() {
    yield takeLatest(ActionConstants.REQUEST_QUERY, handleQueryRequest);
}

function* handleGettingInfo(action) {
    try {
        const user = yield call(getUserInfo, action.id);
        yield put(Actions.infoFetched(user));
    } catch (e) {
        console.error(e);
        yield put(Actions.fetchFailed(e));
    }
}

function* handleUserUpdate(action) {
    try {
        yield call(updateUser, action.data);
        yield put(Actions.userUpdateSuccess());
    } catch (e) {
        console.error(e);
        yield put(Actions.userUpdateFailed(e));
    }
}

function* handleUsersFetch(action) {
    try {
        const users = yield call(getUsersList);
        yield put(Actions.getUsersSuccess(users));
    } catch (e) {
        console.error(e);
        yield put(Actions.getUsersFailed(e));
    }
}

function* handleQueryRequest(action) {
    try {
        const queryResult = yield call(requestQuery, action.query);
        yield put(Actions.queryResultSuccess(queryResult));
    } catch (e) {
        console.error(e);
        yield put(Actions.queryResultFailure(e.message));
        yield delay(2000);
        yield put(Actions.resetQueryStatus());
    }
}


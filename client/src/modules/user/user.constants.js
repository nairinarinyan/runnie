export const GET_INFO = 'user/GET_INFO';
export const INFO_FETCHED = 'user/INFO_FETCHED';
export const FETCH_FAILED = 'user/FETCH_FAILED';

export const UPDATE_USER = 'user/UPDATE_USER';
export const USER_UPDATE_SUCCESS = 'user/USER_UPDATE_SUCCESS';
export const USER_UPDATE_FAILED = 'user/USER_UPDATE_FAILED';

export const GET_USERS_LIST = 'user/GET_USERS_LIST';
export const GET_USERS_SUCCESS = 'user/GET_USERS_SUCCESS';
export const GET_USERS_FAILED = 'user/GET_USERS_FAILED';

export const REQUEST_QUERY = 'user/REQUEST_QUERY';
export const QUERY_RESULT_SUCCESS = 'user/QUERY_RESULT_SUCCESS';
export const QUERY_RESULT_FAILURE = 'user/QUERY_RESULT_FAILURE';
export const RESET_QUERY_STATUS = 'user/RESET_QUERY_STATUS';

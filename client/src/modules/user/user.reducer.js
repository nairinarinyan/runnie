import * as ActionConstants from './user.constants';
import * as AuthConstants from '../auth/auth.constants';

const initialState = {
    queryResult: null,
    error: null
};

export function user(state = initialState, action) {
    switch(action.type) {
        case ActionConstants.USER_UPDATE_SUCCESS:
            return { ...state, updateStatus: 'success' };
        case ActionConstants.GET_USERS_SUCCESS:
            return { ...state, usersList: action.users };
        case ActionConstants.QUERY_RESULT_SUCCESS:
            return { ...state, queryResult: action.result };
        case ActionConstants.QUERY_RESULT_FAILURE:
            return { ...state, queryResult: null, error: action.error };
        case ActionConstants.RESET_QUERY_STATUS:
            return { ...state, error: null };
        default:
            return state;
    }
}

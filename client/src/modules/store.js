import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { fork } from 'redux-saga/effects';

import { layoutReducer } from './layout/index';
import { authSagas, authReducer } from './auth/index';
import { userSagas, userReducer } from './user/index';
import { racesReducer, racesSagas } from './races/index';
import { entriesReducer, entriesSagas } from './entries/index';

function composeSagas(sagas) {
    let forks = [];

    for (let key in sagas) {
        forks.push(fork(sagas[key]));
    }

    return function* rootSaga() {
        yield forks;
    }
}

const reducer = combineReducers(Object.assign({},
    authReducer, userReducer, layoutReducer, racesReducer, entriesReducer
));
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const sagaMiddleware = createSagaMiddleware();
const sagas = Object.assign({}, authSagas, userSagas, racesSagas, entriesSagas);

const store = createStore(reducer, composeEnhancers(
    applyMiddleware(sagaMiddleware)
));

sagaMiddleware.run(composeSagas(sagas));

export default store;
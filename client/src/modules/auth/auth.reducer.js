import * as ActionConstants from './auth.constants';

const initialState = {
    user: null
};

export function auth(state = initialState, action) {
    switch(action.type) {
        case ActionConstants.LOGIN_SUCCESS:
            return { ...state, loginStatus: 'success', user: action.user };
        case ActionConstants.LOGIN_FAILURE:
            return { ...state, loginStatus: 'failure' };
        case ActionConstants.UNIDENTIFIED:
            return { ...state, loginStatus: 'unidentified' };
        case ActionConstants.RESET_LOGIN_STATUS:
            return { ...state, loginStatus: null };
        case ActionConstants.USERNAME_TAKEN:
            return { ...state, invalidUsername: true };
        case ActionConstants.REGISTER_FAILURE:
            return { ...state, registered: false, failMessage: action.error };
        case ActionConstants.REGISTER_SUCCESS:
            return { ...state, registered: true, failMessage: '' };
        case ActionConstants.REQUEST_PASSWORD_RESET_SUCCESS:
            return { ...state, resetPasswordStatus: 'sent' };
        case ActionConstants.REQUEST_PASSWORD_RESET_FAILURE:
            return { ...state, resetPasswordStatus: 'failure' };
        case ActionConstants.RESET_PASSWORD_SUCCESS:
            return { ...state, resetPasswordStatus: 'success' };
        case ActionConstants.RESET_PASSWORD_FAILURE:
            return { ...state, resetPasswordStatus: 'failure' };
        case ActionConstants.GET_CURRENT_USER_SUCCESS:
            return { ...state, loginStatus: 'success', user: action.user };
        default:
            return state;
    }
}

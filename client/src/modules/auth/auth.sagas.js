import { takeLatest, call, put } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import * as ActionConstants from './auth.constants';
import * as Actions from './auth.actions';

import { login, register, resetPassword, requestPasswordReset } from '../../services/auth';
import { getUserInfo } from '../../services/api';

export function* watchLogin() {
    yield takeLatest(ActionConstants.LOGIN, handleLogin);
}

export function* watchRegister() {
    yield takeLatest(ActionConstants.REGISTER, handleRegistration);
}

export function* watchPasswordRestRequest() {
    yield takeLatest(ActionConstants.REQUEST_PASSWORD_RESET, handlePasswordResetRequest);
}

export function* watchResetPassword() {
    yield takeLatest(ActionConstants.RESET_PASSWORD, handlePasswordReset);
}

export function* watchUsernameCheck() {
    yield takeLatest(ActionConstants.CHECK_USERNAME, handleUsernameCheck);
}

export function* watchGetUser() {
    yield takeLatest(ActionConstants.GET_CURRENT_USER, handleGetUser);
}

function* handleLogin(action) {
    try {
        const user = yield call(login, action.fields);
        yield put(Actions.loginSuccess(user));
    } catch (e) {
        yield put(Actions.loginFailure());
        yield delay(1000);
        yield put(Actions.resetLoginStatus());
    }
}

function* handleRegistration(action) {
    try {
        yield call(register, action.fields);
        yield put(Actions.registerSuccess());
    } catch (e) {
        yield put(Actions.registerFailure(e.message));
    }
}

function* handlePasswordResetRequest(action) {
    try {
        yield call(requestPasswordReset, action.email);
        yield put(Actions.resetRequestSuccess());
    } catch (e) {
        yield put(Actions.resetRequestFailure());
    }
}

function* handlePasswordReset({ password, token }) {
    try {
        yield call(resetPassword, password, token);
        yield put(Actions.passwordResetSuccess());
    } catch (e) {
        yield put(Actions.passwordResetFailure());
    }
}

function* handleUsernameCheck({ username }) {
    try {
        yield call(checkUsername, username);
    } catch (e) {
        yield put(Actions.usernameTaken());
    }
}

function* handleGetUser() {
    try {
        const user = yield call(getUserInfo);
        yield put(Actions.getCurrentUserSuccess(user));
    } catch (e) {
        yield put(Actions.unidentified());
    }
}

import * as ActionConstants from './auth.constants';

// login
export function login(fields) {
    return { type: ActionConstants.LOGIN, fields };
}

export function loginSuccess(user) {
    return { type: ActionConstants.LOGIN_SUCCESS, user };
}

export function loginFailure() {
    return { type: ActionConstants.LOGIN_FAILURE };
}

export function unidentified() {
    return { type: ActionConstants.UNIDENTIFIED };
}

export function resetLoginStatus() {
    return { type: ActionConstants.RESET_LOGIN_STATUS };
}

// register
export function register(fields) {
    return { type: ActionConstants.REGISTER, fields };
}

export function registerSuccess() {
    return { type: ActionConstants.REGISTER_SUCCESS };
}

export function registerFailure(error) {
    return { type: ActionConstants.REGISTER_FAILURE, error };
}

// validate
export function checkUsername(username) {
    return { type: ActionConstants.CHECK_USERNAME, username };
}

export function usernameTaken() {
    return { type: ActionConstants.USERNAME_TAKEN };
}

// password request
export function requestPasswordReset(email) {
    return { type: ActionConstants.REQUEST_PASSWORD_RESET, email };
}

export function resetRequestSuccess() {
    return { type: ActionConstants.REQUEST_PASSWORD_RESET_SUCCESS };
}

export function resetRequestFailure() {
    return { type: ActionConstants.REQUEST_PASSWORD_RESET_FAILURE };
}

// reset password
export function resetPassword(password, token) {
    return { type: ActionConstants.RESET_PASSWORD, password, token };
}

export function passwordResetSuccess() {
    return { type: ActionConstants.RESET_PASSWORD_SUCCESS };
}

export function passwordResetFailure() {
    return { type: ActionConstants.RESET_PASSWORD_FAILURE };
}

// current user
export function getCurrentUser() {
    return { type: ActionConstants.GET_CURRENT_USER };
}

export function getCurrentUserSuccess(user) {
    return { type: ActionConstants.GET_CURRENT_USER_SUCCESS, user };
}
import { takeLatest, call, put } from 'redux-saga/effects';
import * as ActionConstants from './entries.constants';
import * as Actions from './entries.actions';

import { createEntry, getEntries, getEntryOfRace, addEntryDetails, submitPaymentData } from '../../services/api';

export function* watchGetEntries() {
    yield takeLatest(ActionConstants.GET_ENTRIES_LIST, handleGetEntries);
}

export function* watchGetEntryOfRace() {
    yield takeLatest(ActionConstants.GET_ENTRY_OF_RACE, handleGetEntryOfRace);
}

export function* watchCreateEntry() {
    yield takeLatest(ActionConstants.CREATE_ENTRY, handleCreateEntry);
}

export function* watchAddEntryDetails() {
    yield takeLatest(ActionConstants.ADD_ENTRY_DETAILS, handleAddEntryDetails);
}

export function* watchSubmitPaymentData() {
    yield takeLatest(ActionConstants.SUBMIT_PAYMENT_DATA, handleSubmitPaymentData);
}

function* handleCreateEntry(action) {
    try {
        const entry = yield call(createEntry, action.data);
        yield put(Actions.createEntrySuccess(entry));
    } catch (e) {
        console.error(e);
        yield put(Actions.createEntryFailed(e));
    }
}

function* handleGetEntries(action) {
    try {
        const entries = yield call(getEntries);
        yield put(Actions.getEntriesSuccess(entries));
    } catch (e) {
        console.error(e);
        yield put(Actions.getEntriesFailed(e));
    }
}

function* handleGetEntryOfRace(action) {
    try {
        const entry = yield call(getEntryOfRace, action.raceId);
        yield put(Actions.getEntryOfRaceSuccess(entry));
    } catch (e) {
        console.error(e);
        yield put(Actions.getEntryOfRaceFailed());
    }
}

function* handleAddEntryDetails(action) {
    try {
        yield call(addEntryDetails, action.entryId, action.data);
    } catch (e) {
        console.error(e);
    }
}

function* handleSubmitPaymentData(action) {
    try {
        yield call(submitPaymentData, action.entryId, action.data);
    } catch (e) {
        console.error(e);
    }
}
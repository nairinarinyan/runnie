import * as ActionConstants from './entries.constants';

function addCreatedEntry(state, { entry }) {
    const newState = { ...state };

    // if (!newState.entriesList.find(e => e.id === entry.id)) {
    //     newState.entriesList.push(entry);
    // }

    return newState;
}

export function entries(state = {}, action) {
    switch(action.type) {
        case ActionConstants.GET_ENTRIES_SUCCESS:
            return { ...state, entriesList: action.entries, activeEntry: null };
        case ActionConstants.GET_ENTRY_OF_RACE_SUCCESS:
            return { ...state, activeEntry: action.entry };
        case ActionConstants.CREATE_ENTRY_SUCCESS:
            return addCreatedEntry(state, action);
        default:
            return state;  
    }
}

import * as ActionConstants from './entries.constants';

export function getEntriesList() {
    return { type: ActionConstants.GET_ENTRIES_LIST };
}

export function getEntriesSuccess(entries) {
    return { type: ActionConstants.GET_ENTRIES_SUCCESS, entries };
}

export function getEntriesFailed(error) {
    return { type: ActionConstants.GET_ENTRIES_FAILED, error };
}

export function getEntryOfRace(raceId) {
    return { type: ActionConstants.GET_ENTRY_OF_RACE, raceId };
}

export function getEntryOfRaceSuccess(entry) {
    return { type: ActionConstants.GET_ENTRY_OF_RACE_SUCCESS, entry };
}

export function getEntryOfRaceFailed() {
    return { type: ActionConstants.GET_ENTRY_OF_RACE_FAILED };
}

export function createEntry(data) {
    return { type: ActionConstants.CREATE_ENTRY, data };
}

export function createEntrySuccess(entry) {
    return { type: ActionConstants.CREATE_ENTRY_SUCCESS, entry };
}

export function createEntryFailed() {
    return { type: ActionConstants.CREATE_ENTRY_FAILED };
}

export function addEntryDetails(entryId, data) {
    return { type: ActionConstants.ADD_ENTRY_DETAILS, entryId, data };
}

export function submitPaymentData(entryId, data) {
    return { type: ActionConstants.SUBMIT_PAYMENT_DATA, entryId, data };
}

export function submitPaymentDataSuccess() {
    return { type: ActionConstants.SUBMIT_PAYMENT_DATA_SUCCESS };
}

export function submitPaymentDataFailed() {
    return { type: ActionConstants.SUBMIT_PAYMENT_DATA_FAILED };
}
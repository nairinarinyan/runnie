import * as entriesReducer from './entries.reducer';
import * as entriesSagas from './entries.sagas';

export { entriesReducer };
export { entriesSagas };

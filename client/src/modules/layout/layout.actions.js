import * as ActionConstants from './layout.constants';

export function setMedia(media) {
    return { type: ActionConstants.SET_MEDIA, media };
}

export function selectMenuItem(selection) {
    return { type: ActionConstants.SELECT_MENU_ITEM, selection };
}

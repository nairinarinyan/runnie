import * as ActionConstants from './layout.constants';

const initialState = {
    media: 'desktop',
    activeState: null
};

export function layout(state = initialState, action) {
    switch(action.type) {
        case ActionConstants.SET_MEDIA:
            return { ...state, media: action.media };
        case ActionConstants.SELECT_MENU_ITEM:
            return { ...state, activeState: action.selection };
        default:
            return state;
    }
}


// Output CommonJS to be used by node too
exports.promisify = (object, method, ret) => {
    return (...args) => {
        return new Promise((resolve, reject) => {
            object[method].call(object, ...args, (err, result) => {
                return err ? reject(err) : resolve(ret ? ret : result);
            });
        });
    };
};

exports.capitalize = string => string[0].toUpperCase() + string.slice(1);

exports.parseQueryString = query => 
    query
        .replace(/\?/, '')
        .split('&')
        .map(part => {
            const [key, value] = part.split('=');
            return { [key] : value };
        })
        .reduce((prev, curr) => Object.assign(prev, curr));

exports.parseSeconds = timeInSecs => {
    const mins = timeInSecs / 60 << 0;
    const secs = timeInSecs - 60 * mins;

    return `${mins}:${secs < 10 ? '0' + secs : secs}`;
}

exports.randomId = () => (Math.random() * (10000 - 5000) + 5000 << 0).toString(16);

function pickOrOmit(object, fields, omit) {
    return Object.entries(object)
        .filter(([key, value]) => omit ? !~fields.indexOf(key) : ~fields.indexOf(key))
        .map(([key, value]) => ({ [key]: value }))
        .reduce((prev, curr) => Object.assign(prev, curr));
}

exports.pick = (object, fields) => pickOrOmit(object, fields);

exports.omit = (object, fields) => pickOrOmit(object, fields, true);
    
exports.parseDateTime = (dateString) => {
    const date = new Date(dateString);

    return `${date.toDateString()}, ${date.toLocaleTimeString()}`;
}

import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Redirect
} from 'react-router-dom';

import PrivateRoute from './PrivateRoute';

import Login from '../containers/Login';
import Register from '../containers/Register';
import Profile from '../containers/Profile';
import Recover from '../containers/Recover';
import ResetPassword from '../containers/ResetPassword';
import Dashboard from '../containers/Dashboard';

import PaymentPage from '../components/PaymentPage';

// Highest level routes
const routes = (
    <Router>
        <Switch>
            <Route path="/login" component={Login} />
            <Route path="/register" component={Register} />
            <Route path="/payment" component={PaymentPage} />
            <Route path="/" render={props => <PrivateRoute props={props} component={Dashboard} />} />
        </Switch>
    </Router>
);

// <Route path="/" component={App}>
//     <IndexRoute component={Dashboard} onEnter={checkAuth} />
//     <Route path="login" component={Login} />
//     <Route path="dashboard" component={Dashboard} onEnter={checkAuth} />
//     <Route path="register" component={Register} />
//     <Route path="recover" component={Recover} />
//     <Route path="reset-password/:token" component={ResetPassword} />
//     <Route path="profile" component={Profile} onEnter={checkAuth}/>
// </Route>

export default routes;

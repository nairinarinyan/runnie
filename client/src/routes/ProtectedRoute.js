import React from 'react';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';

import Spinner from '../components/Spinner';

// Users can access the route only if they have a corresponding role
class ProtectedRoute extends React.Component {
    render() {
    }
}

export default connect(
    ({ auth }) => auth
)(ProtectedRoute);
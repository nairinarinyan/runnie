import React from 'react';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';

import Spinner from '../components/Spinner';

// Users can access the route only if they are logged in
class PrivateRoute extends React.Component {
    render() {
        const {
            component: Component,
            props, user, loginStatus
        } = this.props;

        switch(loginStatus) {
            case 'unidentified':
                return <Redirect to="/login" />;
            case 'success':
                return <Component {...props} />;
            default:
                return <Spinner theme="light" fullscreen />;
        }
    }
}

export default connect(
    ({ auth }) => auth
)(PrivateRoute);
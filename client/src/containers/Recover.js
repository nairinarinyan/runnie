import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../modules/auth/auth.actions';

import AlertInfo from '../components/AlertInfo';
import Input from '../components/Input';
import Toaster from '../components/Toaster';

class Recover extends Component {
    constructor() {
        super();
        this.state = {
            email: ''
        };
    }

    componentWillReceiveProps(props) {
        const { resetPasswordStatus } = props.state.auth;
        this.setState({ resetPasswordStatus });
    }

    onChange({ target }) {
        const { value } = target;
        this.setState({
            email: value,
            resetPasswordStatus: ''
        });
    }

    onSubmit(e) {
        e.preventDefault();
        const { requestPasswordReset } = this.props;
        requestPasswordReset(this.state.email.toLowerCase());
    }

    passwordResetForm() {
        return (
            <div className="form-window">
                <form onSubmit={::this.onSubmit} noValidate>
                    <Input
                        type="email"
                        value={this.state.email}
                        placeholder="Email"
                        onChange={::this.onChange}
                    />
                    <div>
                        <button type="submit">Send password reset link</button>
                    </div>
                </form>
            </div>
        ); 
    }

    pop() {
        if (this.state.resetPasswordStatus === 'failure') return true;
    }

    render() {
        const emailSent = this.props.state.auth.resetPasswordStatus === 'sent';

        return (
            <section id="recover" className={emailSent && 'email-sent'}>
                <Toaster pop={::this.pop} message="No account with such email" duration={2500} type="error"/>
                {emailSent ?
                    <AlertInfo type="password" email={this.state.email} /> :
                    this.passwordResetForm()
                }

            </section>
        );
    }
}

export default connect(
    state => ({state}),
    dispatch => bindActionCreators(actions, dispatch)
)(Recover);
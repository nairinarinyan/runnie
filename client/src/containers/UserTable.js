import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../modules/user/user.actions';

import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn
} from 'material-ui/Table';

class UserTable extends Component {
    componentWillMount() {
        const { usersList, getUsersList } = this.props;
        !usersList && getUsersList();
    }

    renderRows() {
        const { usersList } = this.props;

        return usersList && usersList.map(({ id, username, registrationDate }) =>
            <TableRow className="table-row" key={id}>
                <TableRowColumn>{id}</TableRowColumn>
                <TableRowColumn>{username}</TableRowColumn>
                <TableRowColumn>{new Date(registrationDate).toDateString()}</TableRowColumn>
            </TableRow>
        );
    }

    render() {
        return (
            <section className="main-view">
                <Table className="table">
                    <TableHeader
                        adjustForCheckbox={false}
                        displaySelectAll={false}>
                        <TableRow>
                            <TableHeaderColumn>ID</TableHeaderColumn>
                            <TableHeaderColumn>Username</TableHeaderColumn>
                            <TableHeaderColumn>Date of registration</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody displayRowCheckbox={false}>
                        {this.renderRows()}
                    </TableBody>
                </Table>
            </section>
        );    
    }
}

export default connect(
    ({ user }) => user,
    dispatch => bindActionCreators(userActions, dispatch)
)(UserTable);
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as racesActions from '../modules/races/races.actions';
import { Switch, Route } from 'react-router-dom';

import RacesTable from '../components/RacesTable';
import EntryForm from './EntryForm';
import Confirmation from '../components/Confirmation';

class Races extends Component {
    state = {}

    componentWillMount() {
        this.props.getRaces();
    }

    selectRace(race) {
        this.props.history.push(`/events/${race.id}?step=registration`);
    }

    render() {
        const { racesList, selectedRace, media } = this.props;
        const { start, finish, distance, date } = this.state;
        const onMobile = media === 'mobile';

        return (
            <section className="races main-view">
                <Switch>
                    <Route exact path="/events" render={props =>
                        <RacesTable {...props} races={racesList} selectRow={this.selectRace.bind(this)} />}
                    />
                    <Route exact path="/events/confirmation" component={Confirmation}/>
                    <Route path="/events/:race" render={props =>
                        <EntryForm {...props} onMobile={onMobile} />}
                    />
                </Switch>
            </section>
        );
    }
}

export default connect(
    ({ races, layout }) => ({ ...races, ...layout }),
    dispatch => bindActionCreators(racesActions, dispatch)
)(Races);

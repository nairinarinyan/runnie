import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as userActions from '../modules/user/user.actions';

import TextField from 'material-ui/TextField';

class Profile extends Component {
    render() {
        const { user } = this.props;

        return (
            <section className="main-view profile flex-column">
                <form>
                    <TextField
                        id="profile-username"
                        value={user.username || ''}
                        floatingLabelText="Username"
                        fullWidth={true}
                        onChange={() => {}} />
                    <TextField
                        id="profile-firstname"
                        value={user.firstName || ''}
                        floatingLabelText="First Name"
                        fullWidth={true}
                        onChange={() => {}} />
                    <TextField
                        id="profile-lastname"
                        value={user.lastName || ''}
                        floatingLabelText="Last Name"
                        fullWidth={true}
                        onChange={() => {}} />
                    <TextField
                        id="profile-email"
                        value={user.email || ''}
                        floatingLabelText="Email"
                        fullWidth={true}
                        onChange={() => {}} />
                    <TextField
                        id="profile-phonenumber"
                        value={user.phoneNumber || ''}
                        floatingLabelText="Phone Number"
                        fullWidth={true}
                        onChange={() => {}} />
                </form>
            </section>
        );
    }
}

export default connect(
    ({ auth }) => auth,
    dispatch => bindActionCreators(userActions, dispatch)
)(Profile);

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../modules/user/user.actions';

import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

import Toaster from '../components/Toaster';

class Query extends Component {
    constructor() {
        super();

        this.state = {
            query: ''
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange({ target }) {
        this.setState({
            query: target.value
        });
    }

    onSubmit(e) {
        e.preventDefault();

        const { query } = this.state;
        this.props.requestQuery(query);
    }

    render() {
        const { query } = this.state;
        const { queryResult, error } = this.props;

        return (
            <section className="main-view flex-column">
                <Toaster show={!!error} message={error || ''} type="error"/>

                <h3>SQL Query</h3>
                <form onSubmit={this.onSubmit}>
                    <TextField id="query-input" value={query} onChange={this.onChange} />
                    <RaisedButton type="submit" label="Submit" primary={true} />

                </form>
                <div className="query-result">
                    <pre>
                        {queryResult && JSON.stringify(queryResult, null, 4)}
                    </pre>
                </div>
            </section>
        );
    }
}

export default connect(
    ({ user }) => user,
    dispatch => bindActionCreators(userActions, dispatch)
)(Query);
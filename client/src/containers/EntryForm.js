import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as entriesActions from '../modules/entries/entries.actions';

import { Step, Stepper, StepButton, StepLabel } from 'material-ui/Stepper';

import EntryRegistration from '../components/EntryRegistration';
import EntryDetailsForm from '../components/EntryDetailsForm';
import EntryPayment from '../components/EntryPayment';
import Timer from '../components/Timer';

import { parseQueryString, capitalize } from '../utils';

const steps = ['register', 'details', 'payment'];

class EntryForm extends Component {
    remainingTime = 10 * 60;

    register(data) {
        const { race: raceId } = this.props.match.params;

        const registrationData = { ...data };
        registrationData.raceId = raceId;
        delete registrationData.termsAccepted;

        let submissionTime = new Date();
        submissionTime.setSeconds(submissionTime.getSeconds() - (10 * 60 - this.remainingTime));

        registrationData.submissionTime = submissionTime;

        this.props.createEntry(registrationData);
        this.props.history.push('/events/' + raceId + '?step=' + 'details');
    }

    fillDetails(entryId, detailsData) {
        const { race: raceId } = this.props.match.params;

        this.props.addEntryDetails(entryId, detailsData);
        this.props.history.push('/events/' + raceId + '?step=' + 'payment');
    }

    pay(entryId, paymentData) {
        const { race: raceId } = this.props.match.params;

        this.props.submitPaymentData(entryId, paymentData);
    }

    confirm() {
        this.props.history.push('/entrylist');
    }

    renderContent(stepIndex) {
        const {
            match: { params },
            races: { racesList },
            entries: { activeEntry }
        } = this.props;

        const race = racesList && racesList.find(race => race.id == params.race);

        switch(stepIndex) {
            case 0:
                return <EntryRegistration
                    onSubmit={this.register.bind(this)}
                    race={race} />;
            case 1:
                return <EntryDetailsForm
                    onSubmit={this.fillDetails.bind(this)}
                    getEntryOfRace={this.props.getEntryOfRace}
                    activeEntry={activeEntry}
                    race={race} />;
            case 2:
                return <EntryPayment
                    onSubmit={this.pay.bind(this)}
                    getEntryOfRace={this.props.getEntryOfRace}
                    activeEntry={activeEntry}
                    race={race} />;
        }
    }

    onTimerFinish() {
        const { history } = this.props;
        history.replace('/events');
    }

    onTimerTick(time) {
        this.remainingTime = time;
    }

    render() {
        const { onMobile } = this.props;
        const { search } = this.props.history.location;

        const { step: stepName } = parseQueryString(search);
        let stepIndex = steps.indexOf(stepName);
        
        if (stepIndex === -1) stepIndex = 0;

        return (
            <div className="stepper-container">
                <Stepper activeStep={stepIndex}>
                    {steps.map((step, i) =>
                        <Step key={i}>
                            <StepLabel className={`stepper-label ${onMobile && 'mobile'}`}>
                                {capitalize(step)}
                            </StepLabel>
                        </Step>
                    )}
                </Stepper>
                {stepIndex !== 3 &&
                    <Timer time={this.remainingTime} onFinish={this.onTimerFinish.bind(this)} onTick={this.onTimerTick.bind(this)} />
                }
                {this.renderContent(stepIndex)}
            </div>
        );
    }
}

export default connect(
    state => state,
    dispatch => bindActionCreators(entriesActions, dispatch) 
)(EntryForm)
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as racesActions from '../modules/races/races.actions';

import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import DatePicker from 'material-ui/DatePicker';
import TimePicker from 'material-ui/TimePicker';

import RacesTable from '../components/RacesTable';

const formats = {
    distance: value => value.replace(/[^\d+\.]/g, '')
};

class RacesAdmin extends Component {
    state = {}

    componentWillMount() {
        this.props.getRaces();
    }

    onSubmit(e) {
        e.preventDefault();
        const raceData = { ...this.state };

        const { time } = raceData;
        const hours = time.getHours();
        const minutes = time.getMinutes();
    
        raceData.date.setHours(hours);
        raceData.date.setMinutes(minutes);

        delete raceData.time;

        this.props.createRace(raceData);
    }

    onChange(type, { target: { value } }) {
        const format = formats[type];

        this.setState({
            [type]: format ? format(value) : value
        });
    }

    onDateChange(evt, date) {
        this.setState({ date });
    }

    onTimeChange(evt, time) {
        this.setState({ time });
    }

    selectRace(race) {

    }

    render() {
        const { racesList } = this.props;
        const { name, start, finish, distance, date, time } = this.state;

        return (
            <section className="races main-view flex-column">
                <form className="add-race" onSubmit={this.onSubmit.bind(this)}>
                    <TextField
                        name="race-name-input"
                        value={name || ''}
                        floatingLabelText="Name"
                        onChange={this.onChange.bind(this, 'name')} />
                    <TextField
                        name="race-start-input"
                        value={start || ''}
                        floatingLabelText="Start"
                        onChange={this.onChange.bind(this, 'start')} />
                    <TextField
                        name="race-finish-input"
                        value={finish || ''}
                        floatingLabelText="Finish"
                        onChange={this.onChange.bind(this, 'finish')} />
                    <TextField
                        name="race-distance-input"
                        value={distance || ''}
                        floatingLabelText="Distance"
                        onChange={this.onChange.bind(this, 'distance')} />
                    <DatePicker
                        name="race-date-input"
                        value={date}
                        autoOk={true}
                        floatingLabelText="Date"
                        onChange={this.onDateChange.bind(this)} />
                    <TimePicker
                        name="race-time-input"
                        value={time}
                        autoOk={true}
                        format="24hr"
                        floatingLabelText="Time"
                        onChange={this.onTimeChange.bind(this)} />
                    <RaisedButton type="submit" label="Create race" primary={true} />
                </form>
                <RacesTable
                    selectRace={this.selectRace.bind(this)}
                    races={racesList || []} />
            </section>
        );
    }
}

export default connect(
    ({ races }) => races,
    dispatch => bindActionCreators(racesActions, dispatch)
)(RacesAdmin);

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../modules/auth/auth.actions';

import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';

import Toaster from '../components/Toaster';
import SingleForm from '../components/SingleForm';

class Login extends Component {
    constructor() {
        super();
        this.state = {
            username: '',
            password: ''
        };

        this.onSubmit = this.onSubmit.bind(this);
    }

    componentWillReceiveProps({ user, history }) {
        if (user) history.replace('/');
    }

    onChange(field, { target }) {
        this.setState({
            [field]: target.value,
        });
    }

    onSubmit(e) {
        e.preventDefault();
        const { username, password } = this.state;

        this.props.login({ username, password });
    }

    render() {
        const { history, loginStatus } = this.props;
        const { username, password } = this.state;

        return (
            <main>
                <Toaster show={loginStatus === 'failure'} message="Invalid credentials" type="error"/>

                <SingleForm onSubmit={this.onSubmit}>
                    <TextField
                        floatingLabelText="Username"
                        value={username}
                        onChange={this.onChange.bind(this, 'username')}
                        fullWidth={true} />
                    <TextField
                        floatingLabelText="Password"
                        fullWidth={true}
                        onChange={this.onChange.bind(this, 'password')}
                        type="password" />

                    <RaisedButton
                        className="form-button margin-s-top"
                        label="Login"
                        primary={true}
                        fullWidth={true}
                        type="submit" />

                    <FlatButton
                        onTouchTap={() => history.push('register')}
                        label="register"
                        primary={true}
                        fullWidth={true} />
                    <FlatButton
                        onTouchTap={() => history.push('recover')}
                        label="forgot password?"
                        primary={true}
                        fullWidth={true}  />
                </SingleForm>
            </main>
        );
    }
}

export default connect(
    ({ auth }) => auth,
    dispatch => bindActionCreators(actions, dispatch)
)(Login);
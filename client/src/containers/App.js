import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { setMedia } from '../modules/layout/layout.actions';
import { getCurrentUser } from '../modules/auth/auth.actions';
import { Link } from 'react-router';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import injectTapEventPlugin from 'react-tap-event-plugin';

import '../styles/main.styl';

const muiTheme = getMuiTheme({
    fontFamily: 'Roboto Slab, sans-serif'
});

injectTapEventPlugin();

class App extends Component {
    componentWillMount() {
        const mobile = window.matchMedia("(max-width: 670px)").matches;
        mobile && this.props.setMedia('mobile');

        const { user } = this.props.auth;
        !user && this.props.getCurrentUser();
    }

    render() {
        return (
            <MuiThemeProvider muiTheme={muiTheme}>
                <div>
                    {this.props.children}
                </div>
            </MuiThemeProvider>
        );
    }
}

export default connect(
    state => state,
    dispatch => bindActionCreators({ getCurrentUser, setMedia }, dispatch)
)(App);
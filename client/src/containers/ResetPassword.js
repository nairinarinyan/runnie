import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../modules/auth/auth.actions';

import Input from '../components/Input';

class ResetPassword extends Component {
    constructor() {
        super();
        this.state = {
            password: '',
            passwordRepeat: ''
        };
    }

    componentWillReceiveProps(props) {
        const passwordIsReset = props.state.auth.resetPasswordStatus === 'success';

        if (passwordIsReset) {
            props.router.push('/login');
        }
    }

    onChange(type, { target }) {
        const { value } = target;
        this.setState({
            [type]: value
        });
    }

    onSubmit(e) {
        e.preventDefault();
        const { resetPassword, params: { token } } = this.props;
        const { password, passwordRepeat } = this.state;

        if (password === passwordRepeat) resetPassword(password, token);
    }

    render() {
        return (
            <section id="reset-password">
                <div className="form-window">
                    <form onSubmit={::this.onSubmit} noValidate>
                        <Input
                            type="password"
                            value={this.state.password}
                            placeholder="New password"
                            onChange={this.onChange.bind(this, 'password')}/>
                        <Input
                            type="password"
                            value={this.state.passwordRepeat}
                            placeholder="Repeat new password"
                            onChange={this.onChange.bind(this, 'passwordRepeat')}/>
                        <div>
                            <button type="submit">Reset password</button>
                        </div>
                    </form>
                </div>
            </section>
        );
    }
}

export default connect(
    state => ({state}),
    dispatch => bindActionCreators(actions, dispatch)
)(ResetPassword);
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as entriesActions from '../modules/entries/entries.actions';
import { Switch, Route } from 'react-router-dom'

import EntriesTable from '../components/EntriesTable';
import EntryDetails from '../components/EntryDetails';

class Entries extends Component {
    componentWillMount() {
        this.props.getEntriesList();
    }

    selectEntry(entry) {
        this.props.history.push(`/entrylist/${entry.id}`);
    }

    findEntry(params, entriesList) {
        const { entry: entryId } = params;

        return entriesList && entriesList.find(entry => entry.id == entryId);
    }

    render() {
        const { entriesList } = this.props;

        return (
            <Switch>
                <Route exact path="/entrylist" render={props =>
                    <EntriesTable {...props} entries={entriesList || []} selectEntry={this.selectEntry.bind(this)} />}
                />
                <Route path="/entrylist/:entry" render={props => {
                    const entry = this.findEntry(props.match.params, entriesList);

                    return entry ? <EntryDetails {...props} entry={entry} /> : null;
                }}/>
            </Switch>
        );
    }
}

export default connect(
    ({ entries }) => entries,
    dispatch => bindActionCreators(entriesActions, dispatch)
)(Entries);
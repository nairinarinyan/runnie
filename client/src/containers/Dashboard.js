import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as userActions from '../modules/user/user.actions';
import * as authActions from '../modules/auth/auth.actions';
import * as layoutActions from '../modules/layout/layout.actions';
import { Switch, Route } from 'react-router-dom';

import PeopleIcon from 'material-ui/svg-icons/social/people';
import StorageIcon from 'material-ui/svg-icons/device/storage';
import RunIcon from 'material-ui/svg-icons/maps/directions-run';
import ProfileIcon from 'material-ui/svg-icons/social/person';
import AssignmentIcon from 'material-ui/svg-icons/action/assignment';

import UserTable from './UserTable';
import Query from './Query';
import RacesAdmin from './RacesAdmin';
import Races from './Races';
import Profile from './Profile_new';
import Entries from './Entries';

import DashboardLayout from '../components/DashboardLayout';

const menuItems = {
    admin: [
        { text: 'Races', path: '/events', value: 'races_admin', icon: <RunIcon color="#fff" /> },
        { text: 'Users', path: '/userlist', value: 'users', icon: <PeopleIcon color="#fff" /> },
        { text: 'SQL Query', path: '/sqlquery', value: 'sqlQuery', icon: <StorageIcon color="#fff" /> },
    ],
    standard: [
        { text: 'Profile', path: '/profile', value: 'profile', icon: <ProfileIcon color="#fff" /> },
        { text: 'Races', path: '/events', value: 'races', icon: <RunIcon color="#fff" /> },
        { text: 'Entries', path: '/entrylist', value: 'entries', icon: <AssignmentIcon color="#fff" /> }
    ]
};

class Dashboard extends Component {
    componentDidMount() {
        const { user } = this.props.state.auth;
        const { history } = this.props;

        if (history.location.pathname === '/') {
            history.replace(menuItems[user.role][0].path);
        }
    }

    render() {
        const {
            auth: { user },
            layout: { media }
        } = this.props.state;

        const onMobile = media === 'mobile';

        return (
            <DashboardLayout
                onMobile={onMobile}
                menuItems={user ? menuItems[user.role] : []}>

                <Switch>
                    <Route path="/profile" component={Profile} />
                    <Route path="/userlist" component={UserTable} />
                    <Route path="/sqlquery" component={Query} />
                    <Route path="/events" component={user.role === 'admin' ? RacesAdmin : Races} />
                    <Route path="/entrylist" component={Entries} />
                </Switch>
            </DashboardLayout>
        );
    }
}

export default connect(
    state => ({ state }),
    dispatch => bindActionCreators({ ...userActions, ...authActions, ...layoutActions }, dispatch)
)(Dashboard);
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../modules/auth/auth.actions';

import Input from '../components/Input';
import Toaster from '../components/Toaster';

import { checkUsername, checkRequired } from '../lib/validator';
import { checkPasswordStrength, checkEmail, checkDate, checkPhone } from '../../../lib/validator';

import countries from '../../../data/countries.json';

const fields = [
    { name: 'Username', 'value': 'username', type: 'text', validate: 'unique' },
    { name: 'Id (for legacy user accounts)', 'value': 'id', type: 'number', validate: '' },
    [
        { name: 'First Name', 'value': 'firstName', type: 'text', validate: 'required' },
        { name: 'Last Name', 'value': 'lastName', type: 'text', validate: 'required' }
    ],
    { name: 'Email', 'value': 'email', type: 'email', validate: 'email' },
    { name: 'Password', 'value': 'password', type: 'password', validate: 'password' },
    { name: 'Phone Number', 'value': 'phoneNumber', type: 'tel', validate: 'phone' },
    [
        { name: 'DD', 'value': 'day', type: 'number', validate: 'day' },
        { name: 'MM', 'value': 'month', type: 'number', validate: 'month' },
        { name: 'YYYY', 'value': 'year', type: 'number', validate: 'year' }
    ],
    [
        { name: 'Zip', 'value': 'zip', type: 'text', validate: 'required' },
        { name: 'City', 'value': 'city', type: 'text', validate: 'required' }
    ],
    { name: 'Address', 'value': 'address', type: 'text', validate: 'required' },
    { name: 'Country', 'value': 'country', type: 'select', validate: 'required' }
];

class Register extends Component {
    constructor() {
        super();
        this.state = {
            validation: {
            },
            fields: {
                country: countries[0].name,
            }
        };
    }

    componentWillReceiveProps(props) {
        if (props.state.auth.registered) {
            props.router.replace('/profile');
        }
        const { failMessage } = props.state.auth;
        this.setState({ failMessage });
    }
    
    validate(field, value) {
        const { validation, fields } = this.state;
        let validateFn;

        switch(field.validate) {
            case 'unique':
                validateFn = checkUsername; break;
            case 'required':
                validateFn = checkRequired; break;
            case 'email':
                validateFn = checkEmail; break;
            case 'password':
                validateFn = checkPasswordStrength; break;
            case 'day':
                validateFn = checkDate.bind(this, 'day'); break;
            case 'month':
                validateFn = checkDate.bind(this, 'month'); break;
            case 'year':
                validateFn = checkDate.bind(this, 'year'); break;
            case 'phone':
                validateFn = checkPhone; break;
            default:
                validateFn = () => Promise.resolve();
        }

        return validateFn(value)
            .then(newVal => ({
                field: field.value,
                message: '',
                value: newVal
            }))
            .catch(({ message }) => ({
                field: field.value,
                message
            }));
    }

    validateAll() {
        const { fields: fieldsInState } = this.state;
        const promises = [];

        fields.forEach(field => {
            if (Array.isArray(field)) {
                field.forEach(field => {
                    const value = fieldsInState[field.value];
                    const validatePromise = this.validate(field, value);
                    promises.push(validatePromise);
                })
            } else {
                const value = fieldsInState[field.value];
                this.validate(field, value);
                const validatePromise = this.validate(field, value);
                promises.push(validatePromise);
            }
        });

        return Promise.all(promises)
            .then(validations => {
                const validation = validations
                    .map(v => ({
                        [v.field]: v.message
                    }))
                    .reduce((prev, curr) => Object.assign(curr, prev));

                this.setState({ validation });
                return validation;
            });
    }
    
    setValidationState(validationPromise, field) {
        const { fields, validation } = this.state;

        validationPromise
            .then(({ field, message, value }) => {
                const newState = {
                    validation: {
                        ...validation,
                        [field]: message
                    }
                };

                if (value !== undefined) {
                    Object.assign(newState, {
                        fields: {
                            ...fields,
                            [field]: value
                        }
                    });
                }

                this.setState(newState);
            });
    }

    onChange(field, { target }) {
        const { fields } = this.state;

        const validationPromise = this.validate(field, target.value)
        this.setValidationState(validationPromise, field);

        this.setState({
            fields: {
                ...fields,
                [field.value]: target.value
            },
            failMessage: ''
        });
    }

    onBlur(field) {
        const { fields, validation } = this.state;
        const value = fields[field.value];

        const validationPromise = this.validate(field, value)
        this.setValidationState(validationPromise, field, validation);
    }

    onSubmit(e) {
        e.preventDefault();

        this.validateAll()
            .then(validation => {
                Object.keys(validation).forEach(field => {
                    if (validation[field]) throw new Error('There are validation errors');
                });
            })
            .then(() => {
                const fields = this.setBirthday(this.state);
                this.props.register(fields);
            })
            .catch(console.error);
    }

    setBirthday(state) {
        const fields = { ...state.fields };
        const { day, month, year } = fields;

        fields.birthday = new Date(year, month - 1, day);
        delete fields.year;
        delete fields.month;
        delete fields.day;
        
        return fields;
    }

    renderInput(field) {
        const input = (field, i) => {
            const errorMessage = this.state.validation[field.value];

            return (field.type === 'select' ?
                <select onChange={this.onChange.bind(this, field)}>
                    {countries.map((country, i) =>
                        <option key={i} value={country.name}>{country.name}</option>
                    )}
                </select>
                : <Input
                    key={i}
                    noWrap={true}
                    type={field.type}
                    error={errorMessage}
                    value={this.state.fields[field.value]}
                    placeholder={field.name}
                    onChange={this.onChange.bind(this, field)}
                    onBlur={this.onBlur.bind(this, field)} />
            );
        };

        return Array.isArray(field) ? field.map((field, i) => input(field, i)) : input(field);
    }

    pop() {
        if (this.state.failMessage) return true;
    }

    render() {
        return (
            <section id="register">
                <div className="form-window">
                    <form onSubmit={::this.onSubmit} noValidate>
                        {fields.map((field, i) => {
                            return (
                                <div key={i} className="input-container">
                                    {this.renderInput(field, i)}
                                </div>
                            );
                        })}

                        <div>
                            <button type="submit">Register</button>
                        </div>
                    </form>
                </div>
            </section>
        );
    }
}

export default connect(
    state => ({state}),
    dispatch => bindActionCreators(actions, dispatch)
)(Register);
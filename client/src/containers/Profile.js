import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../modules/user/user.actions';

import AlertInfo from '../components/AlertInfo';
import ProfileInfo from '../components/ProfileInfo';
import Toaster from '../components/Toaster';

class Profile extends Component {
    componentDidMount() {
        const { user } = this.props.state.user;
        if (!user) return this.props.getUserInfo();
    }

    pop() {
        const { updateStatus } = this.props.state.user;
        if (updateStatus === 'success') return true;
    }

    render() {
        const { user } = this.props.state.user;
        const { updateUser } = this.props;

        return (
            <section id="profile">
                <Toaster pop={::this.pop} message="Successfully updated" duration={2000} type="success"/>
                {user &&
                    (user.registrationStatus === 'pending' ?
                    <AlertInfo type="registration" email={user.email} /> :
                    <ProfileInfo updateUser={updateUser} user={user}/>)
                }
            </section>
        );
    }
}

export default connect(
    state => ({ state }),
    dispatch => bindActionCreators(actions, dispatch)
)(Profile);
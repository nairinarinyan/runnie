const webpack = require('webpack');
const path = require('path');
const config = require('./webpack.config.common');

const apiRoutes = [
    '/checkauth',
    '/checkusername',
    '/login',
    '/logout',
    '/register',
    '/recover',
    '/reset-password',
    '/users',
    '/query',
    '/races',
    '/entries'
];
const proxy = {};

apiRoutes.forEach(route => {
    proxy[route] = 'http://localhost:3000/';
});

config.devtool = 'source-map';
config.devServer = {
    host: '0.0.0.0',
    compress: true,
    hot: true,
    historyApiFallback: true,
    proxy
};

config.plugins.push(
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin()
);

module.exports = config;
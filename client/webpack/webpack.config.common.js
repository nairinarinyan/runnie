const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');
const rupture = require('rupture');

const sourcePath = path.resolve('./src');
const destPath = path.resolve('./build');

module.exports = {
    entry: [
        'babel-polyfill',
        'react-hot-loader/patch',
        'whatwg-fetch',
        path.join(sourcePath, 'index.js')
    ],
    output: {
        filename: 'bundle.js',
        path: destPath
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: ['babel-loader'],
                exclude: /node_modules/
            },
            {
                test: /\.styl$/,
                use: ['style-loader', 'css-loader', 'stylus-loader',
                    {
                        loader: 'stylus-loader',
                        options: {
                            use: [rupture()]
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new CopyWebpackPlugin([
            { from: path.join(sourcePath, 'index.html'), to: destPath }
        ])
    ]
};

const fs = require('fs');
const Strategy = require('passport-local').Strategy;
const Sequelize = require('sequelize');
const config = require('../config/config');
const adminData = require('../config/admin-user');
const { registerEntryExpireJob } = require('../lib/cron');

const models = require('../models');
const countryList = require('../data/countries');
const testUserList = require('../data/test_users');

exports.initPassport = passport => {
    const { User } = models.db;

    passport.serializeUser((user, done) => {
        done(null, user.id);
    });

    passport.deserializeUser((id, done) => {
        User.findById(id)
            .then(user => {
                done(null, user);
            })
            .catch(done);
    });

    const strategy = new Strategy((username, passport, done) => {
        User.findOne({ where: { username }})
            .then(user => {
                if (!user) return done(null, false);

                return user.checkPassword(passport)
                    .then(ok => {return ok ? done(null, user) : done(null, false)});
            })
            .catch(done);
    });

    passport.use(strategy);
};

exports.initSequelize = () => {
    const force = config.resetDb === 'true';

    const sequelize = new Sequelize(config.postgresUri, {
        logging: false,
        omitNull: true
    });

    return models.init(sequelize)
        .then(() => sequelize.sync({ force }))
        .then(() => force && fillInitialData(models.db))
        .then(() => sequelize);
};

function fillInitialData(models) {
    const adminUser = models.User.build(adminData);

    return Promise.all([
        adminUser.setPassword(adminData.password).then(adminUser.save.bind(adminUser)),
        models.Country.bulkCreate(countryList),
        models.User.bulkCreate(testUserList)
    ]);
}

exports.initJobs = sequelize => {
    registerEntryExpireJob(() => {
        sequelize.query(`
            
            UPDATE "entries"
            SET "expired" = true
            WHERE
                EXTRACT(EPOCH FROM age(now(),"submissionTime")) > ${10 * 600}
                AND NOT paid AND NOT expired`,

            { type: sequelize.QueryTypes.UPDATE })
            .catch(console.error);
    });
};
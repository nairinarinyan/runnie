const express = require('express');
const path = require('path');
const session = require('express-session');
const passport = require('passport');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const SequelizeStore = require('connect-session-sequelize')(session.Store);

const { port, cookieSecret } = require('./config/config');
const init = require('./init/init');

const app = express();

const cookieOptions = {
    secret: cookieSecret,
    resave: false,
    saveUninitialized: false
};

app.use(bodyParser.json());
app.use(morgan('tiny'));

init.initSequelize()
    .then(sequelize => {
        const api = require('./api/router');
        
        init.initPassport(passport);
        init.initJobs(sequelize);
        
        const sessionStore = new SequelizeStore({ db: sequelize });
        sessionStore.sync();
        cookieOptions.store = sessionStore;

        app.use(session(cookieOptions));
        app.use(passport.initialize());
        app.use(passport.session());

        app.use(api(passport));
        app.listen(port, () => console.log('Listening on %s port', port));
    })
    .catch(console.error);

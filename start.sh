#!/bin/bash

DEBUG=$1

if [ $# -ne 0 ] && [ $DEBUG == 'debug' ]; then
    env $(cat .env) nodemon --inspect-brk index.js
else
    env $(cat .env) nodemon index.js
fi

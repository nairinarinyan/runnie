module.exports = {
    up(queryInterface, Sequelize) {
        return queryInterface.addColumn('races', 'name', Sequelize.STRING);
    },
    down(queryInterface, Sequelize) {
        return queryInterface.removeColumn('races', 'name');
    }
};

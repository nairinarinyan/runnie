#Runnie

### Steps to run locally.
Create an .env file in the root of the project with the env variables for
`EMAIL, EMAIL_PASSWORD, ADMIN_EMAIL, ADMIN_PASSWORD`. Make sure to have postgres up and running.
 
1. `yarn`
2. `cd client && yarn` 
3. `./start.sh` to start the api
    3.1 `RESET_DB=true ./start.sh` if you are starting the first time and want to set up the db
    3.2 `./start.sh debug` to start in debug mode (make sure to have NiM chrome extension installed)

4. `cd client && yarn serve` to get the webpack dev server running

App should be available on the 8080 port.

### Build for deployment etc.

`cd client && yarn build`

Make sure to commit the build files to overcome the problems by heroku (this will be obsolete in near future).
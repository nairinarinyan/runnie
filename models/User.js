const bcrypt = require('bcrypt');
const saltRounds = 8;

module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('user', {
        username: DataTypes.STRING,
        firstName: DataTypes.STRING,
        lastName: DataTypes.STRING,
        email: DataTypes.STRING,
        pendingEmail: DataTypes.STRING,
        phoneNumber: DataTypes.STRING,
        birthday: DataTypes.DATE,
        zip: DataTypes.STRING,
        city: DataTypes.STRING,
        address: DataTypes.STRING,
        passwordHash: DataTypes.STRING,
        registrationStatus: DataTypes.ENUM('confirmed', 'pending'),
        registrationDate: DataTypes.DATE,
        confirmationToken: DataTypes.STRING,
        passwordResetToken: DataTypes.STRING,
        emailUpdateToken: DataTypes.STRING,
        role: {
            type: DataTypes.ENUM('admin', 'standard'),
            defaultValue: 'standard'
        }
    });

    User.associate = function(models) {
        User.belongsTo(models.Country);
        User.belongsToMany(models.Race, { through: models.Entry, as: 'Entries' });
    };

    User.prototype.setPassword = function(plainPassword) {
        return bcrypt.hash(plainPassword, saltRounds)
            .then(hash => this.passwordHash = hash);
    };

    User.prototype.checkPassword = function(plainPassword) {
        return bcrypt.compare(plainPassword, this.passwordHash);
    };

    User.prototype.toJSON = function() {
        delete this.dataValues.passwordHash;
        delete this.dataValues.createdAt;
        delete this.dataValues.updatedAt;
        delete this.dataValues.countryId;
        delete this.dataValues.pendingEmail;
        delete this.dataValues.confirmationToken;
        delete this.dataValues.passwordResetToken;
        delete this.dataValues.emailUpdateToken;
        
        return this.dataValues;
    };

    return User;
};
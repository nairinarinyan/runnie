module.exports = (sequelize, DataTypes) => {
    const Entry = sequelize.define('entry', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        runnerId: DataTypes.STRING,
        prefix: DataTypes.ENUM('Mr', 'Ms', 'Mrs', 'Dr', 'Prof'),
        firstName: DataTypes.STRING,
        lastName: DataTypes.STRING,
        birthday: DataTypes.DATE,
        price: DataTypes.STRING,
        sex: DataTypes.ENUM('Male', 'Female'),
        shirtSize: DataTypes.ENUM('Child', 'XS', 'S', 'M', 'L', 'XL'),
        pace: DataTypes.STRING,
        debut: {
            type: DataTypes.BOOLEAN,
            defaultValue: false   
        },
        bibNumber: DataTypes.STRING,
        phoneNumber: DataTypes.STRING,
        submissionTime: DataTypes.DATE,
        country: DataTypes.STRING,
        city: DataTypes.STRING,
        streetAddress: DataTypes.STRING,
        postalCode: DataTypes.STRING,
        paid: {
            type: DataTypes.BOOLEAN, 
            defaultValue: false
        },
        expired: {
            type: DataTypes.BOOLEAN, 
            defaultValue: false
        }
    }, {
        timestamps: true
    });

    return Entry;
};

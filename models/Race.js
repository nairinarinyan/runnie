module.exports = (sequelize, DataTypes) => {
    const Race = sequelize.define('race', {
        name: DataTypes.STRING,
        start: DataTypes.STRING,
        finish: DataTypes.STRING,
        distance: DataTypes.FLOAT,
        date: DataTypes.DATE
    }, {
        timestamps: false
    });

    Race.associate = function(models) {
        Race.belongsToMany(models.User, { through: models.Entry, as: 'Entries' });
    };

    return Race;
};

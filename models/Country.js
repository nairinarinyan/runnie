module.exports = (sequelize, DataTypes) => {
    const Country = sequelize.define('country', {
        name: DataTypes.STRING,
        code: DataTypes.STRING
    }, {
        timestamps: false
    });

    return Country;
};

const fs = require('fs');
const path = require('path');
const { promisify, capitalize } = require('../utils');

const readdir = promisify(fs, 'readdir');

exports.db = {};

exports.init = sequelize => {
    const { db } = exports;
    db.sequelize = sequelize;

    return readdir(__dirname)
        .then(modelFiles => modelFiles.filter(file => file !== 'index.js'))
        .then(modelFiles => {
            modelFiles.forEach(modelFile => {
                const modelPath = path.join(__dirname, modelFile);
                const model = sequelize.import(modelPath);
                db[capitalize(model.name)] = model;
            });

            Object.keys(db).forEach(modelName => {
                if ('associate' in db[modelName]) {
                    db[modelName].associate(db);
                }
            });

            return db; 
        });
};

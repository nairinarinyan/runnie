exports.shouldBe = role => (req, res, next) => {
    if (!req.user || req.user.role !== role) return res.status(401).end();
    next();
};

exports.checkLogin = (req, res, next) => {
    if (!req.user) return res.status(401).end();
    next();
}
const CronJob = require('cron').CronJob;

exports.registerEntryExpireJob = cb => {
    const entryExpireJob = new CronJob({
        cronTime: '0 * * * * *',
        onTick() {
            cb();
        }
    });

    entryExpireJob.start();
};

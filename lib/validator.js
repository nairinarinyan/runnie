const requiredFields = [
    'username',
    'firstName',
    'lastName',
    'email',
    'password',
    'phoneNumber',
    'zip',
    'city',
    'address',
    'country',
    'birthday'
];

const errorMessages = {
    required: 'This field is required',
    usernameTaken: 'Username is already taken',
    weakPassword: 'Weak password. Should contain uppercase, lowercase and numeric characters, and be >6 characters long',
    invalidEmail: 'Please provide a valid email'
};

exports.errorMessages = errorMessages;

exports.checkUser = function(User, username) {
    return User.findOne({ where: { username: username }})
        .then(function(user) {
            if (user) throw new Error(errorMessages.usernameTaken);
            return;
        });
}

exports.checkPasswordStrength = function(password) {
    if (!password) return Promise.reject(new Error(errorMessages.required));

    // should contain at least one uppercase, one lowercase, one numeric character and be more than 6 chars long
    const passwordStrength = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{6,})/;

    return passwordStrength.test(password) ? Promise.resolve() : Promise.reject( new Error(errorMessages.weakPassword));
}

exports.checkEmail = function(email) {
    if (!email) return Promise.reject(new Error(errorMessages.required));

    const emailRegexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegexp.test(email) ? Promise.resolve() : Promise.reject( new Error(errorMessages.invalidEmail));
}

exports.checkRequiredFields = function(body) {
    return new Promise(function(resolve, reject) {
        requiredFields.forEach(function(field) {
            if (!(field in body)) reject(new Error(errorMessages.required + ': ' + field));
        });
        resolve();
    });
}

function normalizeNumber(value, min, max) {
    value = parseInt(value);
    return value < min ? min : value > max ? max : value;
}

exports.checkDate = function(type, value) {
    return new Promise(function(resolve, reject) {
        if (!value) return reject(new Error(errorMessages.required));

        switch(type) {
            case 'day':
                resolve(normalizeNumber(value, 1, 31)); break
            case 'month':
                resolve(normalizeNumber(value, 1, 12)); break
            case 'year':
                resolve(normalizeNumber(value, 1, 2020)); break
        }
    });
};

exports.checkPhone = function(value) {
    return new Promise(function(resolve, reject) {
        if (!value) return reject(new Error(errorMessages.required));

        resolve(value.replace(/[a-zA-Z]/g, ''));
    });
}

const nodemailer = require('nodemailer');
const uuidV4 = require('uuid/v4');
const config = require('../config/config');

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: config.email,
        pass: config.emailPassword
    }
});

exports.sendConfirmationEmail = user => {
    const confirmationToken = uuidV4();

    const mailOptions = {
        from: '"Confirmator" <sampleappbackend@gmail.com>',
        to: user.email,
        subject: 'Finish your registration',
        text:
`Hi ${user.username},
Confirm and finish your registration by clicking on the link below.
${config.hostname}/register/confirm/${confirmationToken}`
    };

    return new Promise((resolve, reject) => {
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) return reject(error);
            console.log(info);
            resolve(confirmationToken);
        });
    });
}

exports.sendPasswordResetEmail = user => {
    const passwordResetToken = uuidV4();

    const mailOptions = {
        from: '"Password Reset" <sampleappbackend@gmail.com>',
        to: user.email,
        subject: 'Password reset requested',
        text:
`Hi ${user.username},
Click on the link below to reset your password.
${config.hostname}/recover/confirm/${passwordResetToken}`
    };

    return new Promise((resolve, reject) => {
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) return reject(error);
            console.log(info);
            resolve(passwordResetToken);
        });
    });
}

exports.sendEmailUpdateEmail = (username, email) => {
    const updateToken = uuidV4();

    const mailOptions = {
        from: '"Password Reset" <sampleappbackend@gmail.com>',
        to: email,
        subject: 'Email change',
        text:
`Hi ${username},
You've requested to change your email.
Click on the link below to confirm the request.
${config.hostname}/email/confirm/${updateToken}`
    };

    return new Promise((resolve, reject) => {
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) return reject(error);
            console.log(info);
            resolve(updateToken);
        });
    });
}
const config = require('./config');

console.log(config.postgresUri)

module.exports = {
    "development": {
        "dialect": "postgres",
        "url": config.postgresUri
    },
    "production": {
        "dialect": "postgres",
        "url": config.postgresUri
    }
}
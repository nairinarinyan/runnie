module.exports = {
    username: 'admin',
    firstName: 'admin',
    lastName: 'admin',
    email: process.env.ADMIN_EMAIL,
    password: process.env.ADMIN_PASSWORD,
    registrationDate: new Date(),
    role: 'admin'
};
const fs = require('fs');
const path = require('path');

module.exports = {
    clientRoutes: ['/login', '/register', '/profile', '/recover', '/reset-password/*', '/dashboard'],
    port: process.env.PORT || 3000,
    postgresUri: process.env.DATABASE_URL || 'postgres://postgres@localhost/testing',
    cookieSecret: fs.readFileSync(path.join(__dirname, 'cookie-key.pem')).toString(),
    email: process.env.EMAIL,
    emailPassword: process.env.EMAIL_PASSWORD,
    hostname: process.env.HOSTNAME || 'http://localhost:3000',
    resetDb: process.env.RESET_DB || 'false'
};

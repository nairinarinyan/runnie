const { Race } = require('../../models').db;

exports.getRaces = (req, res, next) => {
    Race.findAll() 
        .then(races => res.json(races));
};

exports.createRace = (req, res, next) => {
    Race.create(req.body)
        .then(race => res.status(201).json(race));
};

const { Country } = require('../../models').db;

exports.getCountries = (req, res, next) => {
    Country.findAll()
        .then(countries => res.json(countries));
};

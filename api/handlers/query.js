const { sequelize } = require('../../models').db;

exports.handleQuery = (req, res, next) => {
    const { query } = req.body;

    sequelize
        .query(query)
        .spread((results, meta) => {
            res.json(results);
        })
        .catch(err => {
            res.status(400).json(err);
        });
};

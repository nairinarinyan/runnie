const _ = require('lodash');

const { User, Country } = require('../../models').db;
const { sendConfirmationEmail, sendPasswordResetEmail } = require('../../services/email');
const { checkUser, checkPasswordStrength, checkEmail, checkRequiredFields } = require('../../lib/validator');

function setCountry(user, country) {
    return Country.findOne({
            where: {
                name: country
            }
        })
        .then(countryFound => {
            if (countryFound) {
                user.setCountry(countryFound);
            }
            return user;
        });
}

function performChecks(body) {
    return checkRequiredFields(body)
        .then(() => checkUser(User, body.username))
        .then(() => checkPasswordStrength(body.password))
        .then(() => checkEmail(body.email));
}

function proceedRegistration(req, res, user) {
    if (!user) {
        user = User.build(_.omit(req.body, 'country'));
    } else {
        Object.assign(user, req.body);
    }

    user.registrationStatus = 'pending';
    user.registrationDate = new Date();

    return user.setPassword(req.body.password)
        .then(() => sendConfirmationEmail(user))
        .then(confirmationToken => {
            user.confirmationToken = confirmationToken;
            return user.save();
        })
        .then(() => setCountry(user, req.body.country))
        .then(user => {
            req.login(user, err => {
                if (err) throw new Error();
                res.send(user);
            });
        })
}

function checkBirthdayMatch(user, body) {
    let { birthday } = body;
    birthday = new Date(birthday);

    return user.birthday >= birthday && user.birthday <= birthday;
}

function checkNameMatch(user, body) {
    return user.firstName === body.firstName &&
        user.lastName === body.lastName;
}

exports.register = (req, res, next) => {
    const { body, body: { id }} = req;

    let legacyUser;
    if (id) legacyUser = true;

    if (!legacyUser) return performChecks(body)
        .then(() => proceedRegistration(req, res))
        .catch(err => {
            console.error(err);
            res.status(400).end(err.message);
        });
    
    let user;
    return User.findById(id)
        .then(userFound => {
            if (!userFound) throw new Error('No user with given ID');

            user = userFound;
            const birthdayMatch = checkBirthdayMatch(userFound, body);
            const nameMatch = checkNameMatch(userFound, body);

            if (birthdayMatch || nameMatch) return performChecks(body);
            throw new Error('Birthday or name don\'t match');
        })
        .then(() => proceedRegistration(req, res, user))
        .catch(e => {
            res.status(404).end(e.message);
        });
};

exports.completeRegistration = (req, res, next) => {
    const { confirmationToken } = req.params;

    User.findOne({
            where: { confirmationToken }
        })
        .then(user => {
            if (!user) throw new Error();
            if (user.registrationStatus !== 'pending') throw new Error();

            user.registrationStatus = 'confirmed';
            user.confirmationToken = '';
            return user.save();
        })
        .then(() => res.redirect('/profile'))
        .catch(() => res.redirect('/login'));
};

exports.checkUsername = (req, res, next) => {
    const { username } = req.body;

    checkUser(User, username)
        .then(() => res.end())
        .catch(() => res.status(400).end());
};

exports.checkAuth = (req, res, next) => {
    res.status(!!req.user ? 200 : 401);
    res.json(req.user || {});
};

exports.login = (req, res, next) => {
    res.json(req.user);
};

exports.logout = (req, res, next) => { 
    req.logOut();
    res.redirect('/login');
};

exports.requestPasswordReset = (req, res, next) => {
    const { email } = req.body;

    User.findOne({
            where: { email }
        })
        .then(user => {
            if (!user) throw new Error();

            return sendPasswordResetEmail(user)
                .then(passwordResetToken => {
                    user.passwordResetToken = passwordResetToken;
                    return user.save();
                })
        })
        .then(() => res.end())
        .catch(e => {
            console.error(e);
            res.status(400).end();
        });
};

exports.confirmPasswordReset = (req, res, next) => {
    const { passwordResetToken } = req.params;

    User.findOne({
            where: { passwordResetToken }
        })
        .then(user => {
            if (!user) throw new Error();

            res.redirect('/reset-password/' + passwordResetToken);
        })
        .catch(() => res.redirect('/login'));
};

exports.resetPassword = (req, res, next) => {
    const { token: passwordResetToken, password } = req.body;

    User.findOne({
            where: { passwordResetToken }
        })
        .then(user => {
            if (!user) throw new Error();
            user.passwordResetToken = '';

            return user.setPassword(password).then(user.save.bind(user));
        })
        .then(() => res.redirect('/login'))
        .catch(e => {
            console.error(e);
            res.status(400).end();
        });
};

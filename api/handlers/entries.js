const { Race, User, Entry } = require('../../models').db;
const { pick } = require('../../utils');

function transformEntryResponse(results) {
    return results.map(race => {
        const { entry } = race;

        delete race.entry;
        delete race.dataValues.entry;

        delete entry.dataValues.createdAt;
        delete entry.dataValues.updatedAt;

        entry.race = race;
        entry.dataValues.race = race;

        return entry;
    });
}

function transformGetByResponse(race) {
    const { entry } = race.dataValues.Entries[0];
    delete race.dataValues.Entries;

    entry.dataValues.race = race.dataValues;
    return entry;
}

exports.createEntry = (req, res, next) => {
    const { raceId } = req.body;
    const entryData = Object.assign({}, req.body);

    delete entryData.raceId;

    Race.findById(raceId)
        .then(race => race && req.user.addEntry(race, { through: entryData }))
        .then(entry => res.status(entry ? 201 : 400).json(entry || {}))
        .catch(console.error);
};

exports.getEntries = (req, res, next) => {
    const { user } = req;
    const { by, raceId } = req.query;

    if (by === 'race' && raceId) {
        return Race.findOne({
                where: { id: raceId },
                include: [{
                    model: User,
                    as: 'Entries'
                }]
            })
            .then(race => {
                if (!race) {
                    res.status(404).end();
                } else {
                    res.json(transformGetByResponse(race));
                }
            });
    }

    user.getEntries()
        .then(results => res.json(transformEntryResponse(results)));
};

exports.updateEntry = (req, res, next) => {
    const { id } = req.params;
    const validUpdateFields = ['country', 'city', 'streetAddress', 'postalCode'];
    const updateData = pick(req.body, validUpdateFields);

    Entry.update(updateData, {
            where: {
                id,
                expired: false
            }
        })
        .then(result => {
            res.json({ status: 'success' });
        })
        .catch(e => {
            console.error(e);
            res.status(400).end();
        });

};

exports.handlePaymentRequest = (req, res, next) => {
    const { id } = req.params;
    const { amount } = req.body;

    Entry.findOne({
            where: { id }
        })
        .then(entry => {
            if (!entry || entry.expired) return res.status(400).end();

            return entry.update({ paid: true });
        })
        .then(() => {
            const callbackUrl = encodeURIComponent('/events/confirmation');
            res.redirect(301, '/payment?callbackUrl=' + callbackUrl + '&amount=' + amount);
        })
};

const { User, Country } = require('../../models').db;
const { sendEmailUpdateEmail } = require('../../services/email');

exports.getUsers = (req, res, next) => {
    User.findAll() 
        .then(users => res.json(users));
};

exports.getSelf = (req, res, next) => {
    const { id } = req.user;

    User.findById(id, {
            include: [Country]
        })
        .then(user => {
            res.send(user);
        });
};

exports.getUser = (req, res, next) => {};

exports.updateSelf = (req, res, next) => {
    const { body } = req;

    const updateData = Object.assign({}, body);

    User.findOne({
        where: { id: req.user.id }
    })
    .then(user => {
        if (!user) return res.status(404).end();

        if (user.email !== updateData.email) {
            return sendEmailUpdateEmail(user.username, updateData.email)
                .then(emailUpdateToken => {
                    user.emailUpdateToken = emailUpdateToken;
                    user.pendingEmail = updateData.email;
                    delete updateData.email;
                    return { user, updateData };
                })
        } else {
            return { user, updateData }
        }
    })
    .then(({ user, updateData }) => {
        Object.assign(user, updateData);
        return user.save();
    })
    .then(() => res.end())
    .catch(err => {
        console.error(err);
        res.status(400).end();
    })
};

exports.updateUser = (req, res, next) => {};

exports.confirmEmailUpdate = (req, res, next) => {
    const { emailUpdateToken } = req.params;

    User.findOne({
            where: { emailUpdateToken }
        })
        .then(user => {
            if (!user) throw new Error();

            user.email = user.pendingEmail;
            user.pendingEmail = '';
            user.emailUpdateToken = '';

            return user.save();
        })
        .then(() => res.redirect('/profile'))
        .catch(() => res.redirect('/login'));
};

const express = require('express');
const router = express.Router();
const { resolve } = require('path');

const authHandlers = require('./handlers/auth');
const userHandlers = require('./handlers/users');
const countryHandlers = require('./handlers/countries');
const queryHandlers = require('./handlers/query');
const raceHandlers = require('./handlers/races');
const entryHandlers = require('./handlers/entries');

const { clientRoutes } = require('../config/config');
const { shouldBe, checkLogin } = require('../lib/authorization');

module.exports = passport => {
    router.use(express.static(resolve(__dirname, '../client/build')));

    router
        .post('/register', authHandlers.register)
        .get('/register/confirm/:confirmationToken', authHandlers.completeRegistration);

    router
        .post('/checkusername', authHandlers.checkUsername)
        .get('/checkauth', authHandlers.checkAuth);

    router
        .post('/login', passport.authenticate('local'), authHandlers.login)
        .get('/logout', authHandlers.logout);

    router
        .post('/recover', authHandlers.requestPasswordReset)
        .get('/recover/confirm/:passwordResetToken', authHandlers.confirmPasswordReset)
        .post('/reset-password', authHandlers.resetPassword);

    router
        .get('/users', shouldBe('admin'), userHandlers.getUsers)
        .get('/users/me', checkLogin, userHandlers.getSelf)
        .get('/users/:id', shouldBe('admin'), userHandlers.getUser)
        .put('/users/me', checkLogin, userHandlers.updateSelf)
        .put('/users/:id', shouldBe('admin'), userHandlers.updateUser)
        .get('/email/confirm/:emailUpdateToken', userHandlers.confirmEmailUpdate);

    router.post('/query', shouldBe('admin'), queryHandlers.handleQuery);

    router.get('/countries', countryHandlers.getCountries);

    router
        .get('/races', checkLogin, raceHandlers.getRaces)
        .post('/races', shouldBe('admin'),  raceHandlers.createRace);

    router
        .get('/entries', checkLogin, entryHandlers.getEntries)
        .post('/entries', checkLogin, entryHandlers.createEntry)
        .put('/entries/:id', checkLogin, entryHandlers.updateEntry)
        .post('/entries/:id/payment', checkLogin, entryHandlers.handlePaymentRequest);

    router.get('*', (req, res) => {
        res.sendFile(resolve(__dirname, '../client/build/index.html'));
    });

    return router;
};
